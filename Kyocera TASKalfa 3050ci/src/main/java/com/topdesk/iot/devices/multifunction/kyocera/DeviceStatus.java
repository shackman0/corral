package com.topdesk.iot.devices.multifunction.kyocera;

import com.topdesk.iot.devices.AbstractDeviceStatus;

public class DeviceStatus extends AbstractDeviceStatus {

	/**
	 * for json marshalling use
	 */
	@SuppressWarnings("unused")
	private DeviceStatus() {
		// do nothing
	}

	public DeviceStatus(String resource, String status) {
		super(resource, status);
		if (resource.equals(KyoceraTASKalfa3050ciClient.WASTE_TONER)) {
			this.alert = (getStatus().equals(KyoceraTASKalfa3050ciClient.STATUS_OK) == false);
		} else {
			int levelPercent = Integer.valueOf(getStatus());
			this.alert = levelPercent <= 10;
		}
	}

}
