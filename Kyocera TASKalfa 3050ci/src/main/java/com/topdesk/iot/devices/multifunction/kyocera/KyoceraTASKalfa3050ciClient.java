package com.topdesk.iot.devices.multifunction.kyocera;

import java.util.Collection;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.topdesk.iot.devices.AbstractDevice;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;


/**
 * KyoceraTASKalfa3050ciClient
 * <p>
 */

public class KyoceraTASKalfa3050ciClient extends AbstractDevice {

	public static final String BLACK = "Black";
	public static final String CYAN = "Cyan";
	public static final String MAGENTA = "Magenta";
	public static final String YELLOW = "Yellow";
	public static final String WASTE_TONER = "Waste Toner";
	public static final String STATUS_OK = "OK";

	// web pages
	static String	STATUS_PAGE	= "";

	public KyoceraTASKalfa3050ciClient() {
		super();
	}

	@Override
	protected String getStatusPagePath() {
		return STATUS_PAGE;
	}

	/**
	 * we check the following statuses:
	 * 1. black toner
	 * 2. cyan toner
	 * 3. magenta toner
	 * 4. yellow toner
	 */
	@Override
	protected Collection<IDeviceStatus> extractDeviceStatuses(Document dom) {
		Element tonerFrame = dom.getElementById("tonerid");
		Element contentRowTable = tonerFrame.getElementById("contentrow");
		Collection<IDeviceStatus> deviceStatuses = new HashSet<>();
		deviceStatuses.add(extractTonerStatus(BLACK, contentRowTable));
		deviceStatuses.add(extractTonerStatus(CYAN, contentRowTable));
		deviceStatuses.add(extractTonerStatus(MAGENTA, contentRowTable));
		deviceStatuses.add(extractTonerStatus(YELLOW, contentRowTable));
		deviceStatuses.add(extractWasteTonerStatus(contentRowTable));

		return deviceStatuses;
	}

	IDeviceStatus extractTonerStatus(String colorText, Element contentRowTable) {
		String status = "0";
		Elements trElements = contentRowTable.getElementsByTag("tbody").get(0).getElementsByTag("tr");
		outer:
		for (Element trElement: trElements) {
			Elements tdElements = trElement.getElementsByTag("td");
			int i;
			for (i = 0; i < tdElements.size(); i++) {
				Element tdElement = tdElements.get(i);
				if (tdElement.text().contains(colorText)) {
					// this is the tr we want
					i += 4;
					status = tdElements.get(i).text().replace((char)160,' ').replace('%',' ').trim();     // &nbsp; = char 160
					break outer;
				}
			}
		}
		return new DeviceStatus(colorText, status);
	}

	IDeviceStatus extractWasteTonerStatus(Element contentRowTable) {
		String status = "0";
		Elements trElements = contentRowTable.getElementsByTag("tbody").get(0).getElementsByTag("tr");
		outer:
		for (Element trElement: trElements) {
			Elements tdElements = trElement.getElementsByTag("td");
			int i;
			for (i = 0; i < tdElements.size(); i++) {
				Element tdElement = tdElements.get(i);
				if (tdElement.text().contains(WASTE_TONER)) {
					// this is the tr we want
					i += 1;
					status = tdElements.get(i).text().replace((char)160,' ').trim();     // &nbsp; = char 160
					break outer;
				}
			}
		}
		return new DeviceStatus(WASTE_TONER, status);
	}

	@Override
	protected String getProtocol() {
		return HTTP;
	}

	private static final String MANUFACTURER = "Kyocera";
	private static final String MODEL = "Taskalfa 3050 CI";

	@Override
	public boolean canQueryThisDevice(IDeviceInfo<IDeviceStatus> deviceInfo) {
		return ((deviceInfo.getManufacturer().equals(MANUFACTURER)) && (deviceInfo.getModel().equals(MODEL)));
	}

}
