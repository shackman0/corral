package com.topdesk.iot.devices.printers.hplaserjet;

import java.util.Collection;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.topdesk.iot.devices.AbstractDevice;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;

public class HPLaserJetClient extends AbstractDevice {

	// web pages
	static String	INK_LEVELS			= "hp/device/info_deviceStatus.html";

	public HPLaserJetClient() {
		super();
	}

	@Override
	protected String getStatusPagePath() {
		return INK_LEVELS;
	}

	@Override
	protected Collection<IDeviceStatus> extractDeviceStatuses(Document dom) {
		Collection<IDeviceStatus> deviceStatuses = new HashSet<>();
		Elements hpGasGaugeBorderElements = dom.getElementsByClass("hpGasGaugeBorder");
		Element tonerLevelElement = hpGasGaugeBorderElements.select("td").get(0);
		String styleAttr = tonerLevelElement.attr("style");
		int end = styleAttr.indexOf("%");
		String tonerLevelPercent = styleAttr.substring("WIDTH:".length(), end);
		deviceStatuses.add(new DeviceStatus("toner level %", tonerLevelPercent));

		return deviceStatuses;
	}

	private static final String MANUFACTURER = "Hewlett Packard";
	private static final String MODEL_PREFIX = "LaserJet ";
	private static final String MODEL_CONTAINS = " MFP ";

	@Override
	public boolean canQueryThisDevice(IDeviceInfo<IDeviceStatus> deviceInfo) {
		return ((deviceInfo.getManufacturer().equals(MANUFACTURER)) && (deviceInfo.getModel().startsWith(MODEL_PREFIX)) && (deviceInfo.getModel().contains(MODEL_CONTAINS)));
	}

}
