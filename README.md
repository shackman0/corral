this software belongs to Floyd Shackelford. 

you are allowed to scrutinize this software for the purposes of evaluating my skills as a software engineer. 
any other use requires advance permission.

Corral as in "horse corral"

when i was at topdesk, i started experimenting with developing an IoT architecture. TopDesk is a trouble-ticket system for facilities maintenance.
in our office, we had a lot of printers scattered throughout the building. these printers had a habit of being unusable because they would run out
of toner, paper, etc.

on my own initiative and time, i wrote "corral" to poll all the printers for status and created a trouble ticket for the IT guys whenever it was in a non-operational state. this
solved two problems:

1. the IT guys would learn about a problem when it happened and not after 20 people ignored the printer and used another printer before someone would 
alert them to the problem
2. with the intent that the printers would be operational at all times, which should make a lot of people happy.