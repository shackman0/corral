#!/bin/bash

java \
  -Dcom.topdesk.remotebroker.host=pc1795 \
  -Dcom.topdesk.iot.subscriber.topdeskapi.rooturl="http://sneaks.topdesk.com:751/tas/api" \
  -Dcom.topdesk.iot.topdeskapi.incident.category="Hardware" \
  -Dcom.topdesk.iot.topdeskapi.incident.subcategory="Workstation" \
  -Dcom.topdesk.iot.topdeskapi.incident.operator.surname="PRINTERMONITOR" \
  -Dcom.topdesk.iot.topdeskapi.incident.operatorgroup="IT Services" \
  -Dcom.topdesk.iot.topdeskapi.incident.person.surname="PRINTERMONITOR" \
  -jar ./bin/CreateIncidentAction.jar \
| tee ./logs/$(date +"%Y_%m_%d_%I_%M_%p").log
