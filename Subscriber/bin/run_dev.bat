@echo off

rem you must create a PRINTERMONITOR operator and person on sneaks before running this

java ^
	-Dcom.topdesk.remotebroker.host=pc1795 ^
	-Dcom.topdesk.iot.subscriber.topdeskapi.rooturl="http://sneaks.topdesk.com:751/tas/api" ^
	-Dcom.topdesk.iot.topdeskapi.incident.category="Hardware" ^
	-Dcom.topdesk.iot.topdeskapi.incident.subcategory="Workstation" ^
	-Dcom.topdesk.iot.topdeskapi.incident.operator.surname="PRINTERMONITOR" ^
	-Dcom.topdesk.iot.topdeskapi.incident.operatorgroup="IT Services" ^
	-Dcom.topdesk.iot.topdeskapi.incident.person.surname="PRINTERMONITOR" ^
	-Dcom.topdesk.iot.messaging.durable="true" ^
	-Dcom.topdesk.iot.messaging.clientid="CreateIncidentAction" ^
	-Dcom.topdesk.iot.subscriber.subscriptionname="CreateIncidentAction" ^
  -jar ./bin/CreateIncidentAction.jar
