#!/bin/bash

# you must run this as the postgres super user

SUPERUSER=postgres
SUPERPASSWORD=postgres

if [ "$USER" != "$SUPERUSER" ]; then
	echo "you must run this command as the postgresql super user, e.g.: sudo -u $SUPERUSER ./initdb.sh";
	exit 1;
fi


# set up

HOST=pc1795
PORT=5432
USER=corral
ROLE=corral
PASSWORD=corral
DATABASE=corral_device_incident

DEVICE_INFO_TABLE=device_info
DEVICE_STATUS_TABLE=device_status


# do it
psql -h $HOST -p $PORT <<EOF
drop database if exists $DATABASE;
drop role if exists $USER;

create role $USER nocreatedb nocreaterole nocreateuser noinherit login noreplication nobypassrls password '$PASSWORD' ;
create database $DATABASE owner=$USER ;
EOF

psql -d $DATABASE -U $USER -h $HOST -p $PORT <<EOF

drop table if exists device_incident cascade ;

create table device_incident (
	id                     uuid      not null primary key,
	incident_id            uuid      not null,
	incident_number        text      not null );

create table device_info (
	id                     uuid      not null primary key,
	device_incident_id     uuid      not null,
	ignore                 boolean   not null,
	host                   text      not null,
	object_id              text      not null,
	branch_office          text      not null,
	manufacturer           text      not null,
	model                  text      not null );

create table device_status (
	id                     uuid      not null primary key,
	device_info_id         uuid      not null,
	resource               text      not null,
	status                 text      not null,
	alert                  boolean   not null );

alter table device_info add constraint device_incident_id foreign key (device_incident_id) references device_incident (id) on delete cascade ;
alter table device_status add constraint device_info_id foreign key (device_info_id) references device_info (id) on delete cascade ;

EOF
