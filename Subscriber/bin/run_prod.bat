@echo off

java ^
	-Dcom.topdesk.remotebroker.host="pc1795" ^
	-Dcom.topdesk.iot.subscriber.topdeskapi.rooturl="https://toperations-acc.topdesk.com/tas/api" ^
	-Dcom.topdesk.iot.topdeskapi.incident.category="ICT" ^
	-Dcom.topdesk.iot.topdeskapi.incident.subcategory="Printers" ^
	-Dcom.topdesk.iot.topdeskapi.incident.operator.surname="ICT Delft" ^
	-Dcom.topdesk.iot.topdeskapi.incident.operatorgroup="ICT Delft" ^
	-Dcom.topdesk.iot.topdeskapi.incident.person.surname="ICT Delft" ^
  -jar ./bin/CreateIncidentAction.jar
