package com.topdesk.iot.message.consumer;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;
import com.topdesk.iot.devices.dto.DeviceInfoDto;
import com.topdesk.iot.dtos.CategoryDto;
import com.topdesk.iot.dtos.IncidentDto;
import com.topdesk.iot.dtos.OperatorDto;
import com.topdesk.iot.dtos.OperatorGroupDto;
import com.topdesk.iot.dtos.PersonDto;
import com.topdesk.iot.dtos.SubCategoryDto;
import com.topdesk.iot.dtos.UrgencyDto;
import com.topdesk.iot.message.consumer.history.DeviceIncidentException;
import com.topdesk.iot.message.consumer.history.DeviceIncidents;
import com.topdesk.jdbc.EnhancedSQLException;
import com.topdesk.rest.RestApiException;
import com.topdesk.restapi.IncidentApiClient;
import com.topdesk.restapi.IncidentCategoryApiClient;
import com.topdesk.restapi.IncidentSubCategoryApiClient;
import com.topdesk.restapi.LoginAPIClient;
import com.topdesk.restapi.OperatorApiClient;
import com.topdesk.restapi.OperatorGroupApiClient;
import com.topdesk.restapi.PersonApiClient;
import com.topdesk.restapi.UrgencyApiClient;
import com.topdesk.util.LogMessage;
import com.topdesk.util.OutParameter;

/**
 * CreateIncident
 * <p>
 * Creates an incident from a CreateIncidentAction message.
 * For devices having one or more error conditions, creates an incident to have the device serviced for all the error conditions.
 */

public class CreateIncident {

  static final Logger LOG = LoggerFactory.getLogger(CreateIncident.class);

	private DeviceIncidents deviceIncidents;

	public CreateIncident() throws SQLException {
		this.deviceIncidents = new DeviceIncidents();
	}

	/**
	 * create an incident if the device needs servicing and no other incident created by this service is open for this device-and-resource. otherwise, do nothing.
	 *
	 * @param deviceInfo
	 * @throws RestApiException
	 *
	 * todo: what if someone has already submitted an incident manually for this device-and-resource?
	 * @throws UnsupportedEncodingException
	 * @throws DeviceIncidentException
	 * @throws RestApiException
	 * @throws EnhancedSQLException
	 */
	public void submitIncidentIfNeeded(IDeviceInfo<? extends IDeviceStatus> deviceInfo) throws RestApiException, EnhancedSQLException, UnsupportedEncodingException, DeviceIncidentException {
		String authenticationToken = LoginAPIClient.loginSubscriber();
		if (deviceInfo.isAlert()) {
			Collection<IDeviceStatus> reportableDeviceStatuses = deviceIncidents.getReportableDeviceStatuses(authenticationToken, deviceInfo);
			if (reportableDeviceStatuses.isEmpty() == false) {
				// build the the incident

				StringBuilder request = new StringBuilder();
				request.append(String.format("%s: ", deviceInfo.getObjectId()));
				for (IDeviceStatus deviceStatus : deviceInfo.getDeviceStatuses()) {
					if (deviceStatus.isAlert() == true) {
						request.append(String.format("%s = %s, ", deviceStatus.getResource(), deviceStatus.getStatus()));
					}
				}

				CategoryDto categoryDto = new IncidentCategoryApiClient().findUnique(authenticationToken, Configuration.INCIDENT_CATEGORY.value());
				SubCategoryDto subCategoryDto = new IncidentSubCategoryApiClient().findUnique(authenticationToken, Configuration.INCIDENT_SUB_CATEGORY.value());
				OperatorDto operatorDto = new OperatorApiClient().findUnique(authenticationToken, Configuration.INCIDENT_OPERATOR_SURNAME.value(), Configuration.INCIDENT_OPERATOR_FIRSTNAME.value());
				OperatorGroupDto operatorGroupDto = new OperatorGroupApiClient().findUnique(authenticationToken, Configuration.INCIDENT_OPERATOR_GROUP.value());
//				ObjectDto objectDto = new ObjectApiClient().findUnique(authenticationToken, deviceInfo.getObjectId());
				UrgencyDto urgencyDto = new UrgencyApiClient().findUnique(authenticationToken, Configuration.INCIDENT_URGENCY.value());
				PersonDto personDto = new PersonApiClient().findUnique(authenticationToken, Configuration.INCIDENT_PERSON_SURNAME.value(), Configuration.INCIDENT_PERSON_FIRSTNAME.value());

				IncidentDto incidentDto = IncidentDto
					.builder()
					.action("fix it")
					.briefDescription(String.format("%s needs servicing", deviceInfo.getObjectId()))
					.callDate(new Date())
					.category(categoryDto)
//					.object(objectDto)
					.operator((operatorDto == null) ? operatorGroupDto : operatorDto)
					.operatorGroup(operatorGroupDto)
					.caller(personDto)
					.request(request.toString())
					.status("firstLine")
					.subcategory(subCategoryDto)
					.targetDate(new Date())
					.urgency(urgencyDto)
					.build();

				// invoke the incident API
				OutParameter<String> requestJson = new OutParameter<>();
				OutParameter<String> response = new OutParameter<>();

				IncidentApiClient.submitIncident(authenticationToken, incidentDto, requestJson, response);
				String incidentJson = response.getPayload();
				JsonObject jsonObject;
				try (JsonReader jsonReader = Json.createReader(new StringReader(incidentJson))) {
					jsonObject = jsonReader.readObject();
				}
				DeviceInfoDto deviceInfoDto = new DeviceInfoDto(deviceInfo);
				String incidentId = jsonObject.getString("id");
				String incidentNumber = jsonObject.getString("number");
				deviceIncidents.addDeviceIncident(incidentId, incidentNumber, deviceInfoDto);
				LOG.info(LogMessage.msg(String.format("Incident \"%s\" (%s) created for %s", incidentNumber, incidentId, request)));
			}
		}
	}

}
