package com.topdesk.iot.message.consumer.history;

import java.net.HttpURLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

import javax.json.JsonObject;

import org.apache.http.StatusLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;
import com.topdesk.jdbc.EnhancedSQLException;
import com.topdesk.rest.RestApiException;
import com.topdesk.restapi.IncidentApiClient;
import com.topdesk.util.LogMessage;
import com.topdesk.util.OutParameter;

/**
 * DeviceIncidents
 * <p>
 * The persistent set of all previously reported DeviceInfo-DeviceStatus tuples We use this so we don't repeatedly react to the same Action-able DeviceStatus alert
 */

public class DeviceIncidents implements AutoCloseable {

	private static final Logger	LOG					= LoggerFactory.getLogger(DeviceIncidents.class);

	public static final String	DATABASE		= "corral_device_incident";

	private Connection					connection	= null;

	/**
	 * use this in a try-with-resources block
	 */
	public DeviceIncidents() {
		// do nothing
	}

	@Override
	public void close() throws Exception {
		if (connection != null) {
			if (connection.isClosed() == false) {
				connection.close();
			}
			connection = null;
		}
	}

	/**
	 * lazy load
	 *
	 * @return
	 * @throws DeviceIncidentException
	 * @throws SQLException
	 */
	public Collection<DeviceIncidentEntity> fetchDeviceIncidents() throws EnhancedSQLException, DeviceIncidentException {
		Collection<DeviceIncidentEntity> deviceIncidents = DeviceIncidentJdbc.queryList(getConnection());

		return deviceIncidents;
	}

	/**
	 * lazy connect
	 *
	 * @return
	 * @throws SQLException
	 */
	private Connection getConnection() throws EnhancedSQLException {
		try {
			if ((connection == null) || (connection.isClosed())) {
				connection = DriverManager.getConnection(Configuration.SUBSCRIBER_JDBC_CONNECTION.value(), Configuration.JDBC_USER.value(), Configuration.JDBC_PASSWORD.value());
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(Configuration.SUBSCRIBER_JDBC_CONNECTION.value()).build();
		}

		return connection;
	}

	public DeviceIncidentEntity addDeviceIncident(String incidentUuid, String incidentNumber, IDeviceInfo<IDeviceStatus> deviceInfo) throws EnhancedSQLException {
		String deviceIncidentId = UUID.randomUUID().toString();
		DeviceInfoEntity deviceInfoEntity = new DeviceInfoEntity(deviceIncidentId, deviceInfo);
		DeviceIncidentEntity deviceIncident = DeviceIncidentEntity.builder().id(deviceIncidentId).incidentUuid(incidentUuid).incidentNumber(incidentNumber).deviceInfo(deviceInfoEntity).build();
		DeviceIncidentJdbc.insert(getConnection(), deviceIncident);

		return deviceIncident;
	}

	/**
	 * determines which, if any, of the DeviceInfo's DeviceStatus-es need to be reported as an incident we use this so we don't repeatedly respond to the same deviceInfo-deviceStatus tuple
	 * <ol>
	 * <li>a non-alert DeviceInfo is quietly ignored
	 * <li>non-alert DeviceStatus-es are ignored
	 * <li>existing DeviceInstances' incidents for this objectId are checked. if the DeviceIncidentEntity's incident is closed, then it's DeviceIncidentEntity is purged. the deviceInfo-deviceStatus
	 * tuple is potentially a new alert.
	 * <li>existing deviceInfo-deviceStatus tuples that remain and exist already as DeviceInstances are quietly ignored. they already have open incidents.
	 * <li>those deviceInfo-deviceStatus tuples that remain and are not already reported are new alerts
	 * <ol>
	 * <li>they should be reported, and
	 * <li>a DeviceIncidentEntity should be created
	 * <ol>
	 * <ol>
	 *
	 * @return Collection<IDeviceStatus> that need to be reported for this DeviceInfo
	 * @throws RestApiException
	 * @throws EnhancedSQLException
	 * @throws DeviceIncidentException
	 */
	public Collection<IDeviceStatus> getReportableDeviceStatuses(String authenticationToken, IDeviceInfo<? extends IDeviceStatus> deviceInfo) throws EnhancedSQLException, RestApiException, DeviceIncidentException {
		Collection<IDeviceStatus> getReportableDeviceStatuses = new HashSet<>();
		// a non-alert DeviceInfo is quietly ignored
		if (deviceInfo.isAlert()) {
			// remove DeviceIncidents for this objectId whose incidents are closed
			OutParameter<StatusLine> statusLineCarrier = new OutParameter<>();
			for (DeviceIncidentEntity deviceIncidentHavingObjectId : DeviceIncidentJdbc.queryObjectId(getConnection(), deviceInfo.getObjectId())) {
				purgeDeviceIncidentIfIncidentIsNoLongerActive(authenticationToken, deviceIncidentHavingObjectId, true, statusLineCarrier);
			}
			for (IDeviceStatus deviceStatus : deviceInfo.getDeviceStatuses()) {
				// non-alert DeviceStatus-es are ignored
				if (deviceStatus.isAlert()) {
					// do we have an existing DeviceStatus for this deviceInfo-deviceStatus tuple?
					Collection<DeviceIncidentEntity> deviceIncidentsHavingObjectResource = DeviceIncidentJdbc.queryObjectIdResource(getConnection(), deviceInfo.getObjectId(), deviceStatus.getResource());
					if (deviceIncidentsHavingObjectResource.isEmpty()) {
						// this is a new alert that has not been reported yet.
						getReportableDeviceStatuses.add(deviceStatus);
					} else {
						// there is at least one open incident for this deviceInfo-deviceStatus tuple. we won't report this tuple for now. the open incident may clean this up as well.
						deviceIncidentsHavingObjectResource.forEach(deviceIncident -> LOG.info(LogMessage.msg(String.format("Incident \"%s\" (%s) is currently open for device \"%s\" resource \"%s\"", deviceIncident.getIncidentNumber(), deviceIncident.getIncidentUuid(), deviceInfo.getObjectId(), deviceStatus.getResource()))));
					}
				}
			}
		}

		return getReportableDeviceStatuses;
	}

	/**
	 * go through the DeviceIncidents and delete any that have been closed it is recommended that you call PurgeClosedIncidents() before calling a batch of add()'s
	 *
	 * @throws EnhancedSQLException
	 * @throws DeviceIncidentException
	 */
	public void purgeClosedDeviceIncidents(String authenticationToken) throws EnhancedSQLException, DeviceIncidentException {
		OutParameter<StatusLine> statusLineCarrier = new OutParameter<>();
		for (DeviceIncidentEntity deviceIncident : fetchDeviceIncidents()) {
			try {
				purgeDeviceIncidentIfIncidentIsNoLongerActive(authenticationToken, deviceIncident, true, statusLineCarrier);
			} catch (RestApiException e) {
				e.printStackTrace();
				continue;
			}
		}
	}

	/**
	 * @param authenticationToken
	 * @param deviceIncident
	 * @return true: incident is closed; device incident purged. false: incident was either not found (for whatever reason, including network problems) or was found and not closed; device incident
	 *         unchanged.
	 * @throws RestApiException
	 * @throws EnhancedSQLException
	 */
	private boolean purgeDeviceIncidentIfIncidentIsNoLongerActive(String authenticationToken, DeviceIncidentEntity deviceIncident, boolean purgeIfNotFound, OutParameter<StatusLine> statusLineCarrier) throws RestApiException, EnhancedSQLException {
		boolean purgeDeviceIncidentEntity = false;

		OutParameter<JsonObject> jsonObjectCarrier = new OutParameter<>();
		StatusLine statusLine = IncidentApiClient.getIncidentAsJsonObject(authenticationToken, deviceIncident.getIncidentUuid(), jsonObjectCarrier);
		statusLineCarrier.setPayload(statusLine);

		if (purgeIfNotFound && (statusLine.getStatusCode() == HttpURLConnection.HTTP_NOT_FOUND)) {
			purgeDeviceIncidentEntity = true;
		} else
		if (statusLine.getStatusCode() == HttpURLConnection.HTTP_OK) {
			JsonObject jsonObject = jsonObjectCarrier.getPayload();
			Boolean completed = jsonObject.getBoolean("completed");
			purgeDeviceIncidentEntity = completed;
		}

		if (purgeDeviceIncidentEntity == true) {
			DeviceIncidentJdbc.delete(getConnection(), deviceIncident.getId());
			LOG.info(LogMessage.msg(String.format("purged DeviceIncidentEntity for device \"%s\", incident \"%s\"", deviceIncident.getDeviceInfo().getObjectId(), deviceIncident.getIncidentNumber())));
		}

		return purgeDeviceIncidentEntity;
	}

}
