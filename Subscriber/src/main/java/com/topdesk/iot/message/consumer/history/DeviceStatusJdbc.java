package com.topdesk.iot.message.consumer.history;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.jdbc.EnhancedSQLException;

public class DeviceStatusJdbc {

	static final Logger					LOG									= LoggerFactory.getLogger(DeviceStatusJdbc.class);

	private static final String	DEVICE_STATUS_TABLE	= "device_status";

	private static final String	INSERT_FORMAT				= "insert into " + DEVICE_STATUS_TABLE + " (id, device_info_id, resource, status, alert) values ('%s', '%s', '%s', '%s', '%s')";
	private static final String	FETCH_FORMAT				= "select id, device_info_id, resource, status, alert from " + DEVICE_STATUS_TABLE + " where id = '%s'";
	private static final String	QUERY_FORMAT				= "select id, device_info_id, resource, status, alert from " + DEVICE_STATUS_TABLE + " where device_info_id = '%s'";

	public static void insert(Connection connection, DeviceStatusEntity deviceStatus) throws SQLException {
		String sql = String.format(INSERT_FORMAT, deviceStatus.getId(), deviceStatus.getDeviceInfoId(), deviceStatus.getResource(), deviceStatus.getStatus(), String.valueOf(deviceStatus.isAlert()));
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(sql);
		}
	}

	/**
	 * read the device status from the database
	 *
	 * @param connection
	 * @param DeviceStatusUuid
	 * @return the DeviceStatusEntity or null if not found
	 * @throws SQLException
	 */
	public static DeviceStatusEntity fetch(Connection connection, String DeviceStatusUuid) throws EnhancedSQLException {
		DeviceStatusEntity deviceStatus = null;
		String sql = String.format(FETCH_FORMAT, DeviceStatusUuid);
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(sql);
			boolean found = resultSet.next();
			if (found) {
				deviceStatus = new DeviceStatusEntity(resultSet);
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}

		return deviceStatus;

	}

	/**
	 * return the DeviceStatusEntity-s that belong to the deviceInfo
	 *
	 * @param connection
	 * @param deviceInfoId
	 * @return
	 * @throws SQLException
	 */
	public static Collection<DeviceStatusEntity> fetchForDeviceInfo(Connection connection, String deviceInfoId) throws EnhancedSQLException {
		Collection<DeviceStatusEntity> results = new ArrayList<>();
		String sql = String.format(QUERY_FORMAT, deviceInfoId);
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				DeviceStatusEntity deviceStatus = new DeviceStatusEntity(resultSet);
				results.add(deviceStatus);
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}

		return results;
	}

}
