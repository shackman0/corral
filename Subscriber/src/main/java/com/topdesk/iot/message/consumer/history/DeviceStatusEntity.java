package com.topdesk.iot.message.consumer.history;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.topdesk.iot.devices.IDeviceStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeviceStatusEntity implements IDeviceStatus {

	String											id;
	String											deviceInfoId;

	String											resource;
	String											status;
	boolean											alert;

	public DeviceStatusEntity(ResultSet resultSet) throws SQLException {
		this.id = resultSet.getString("id");
		this.deviceInfoId = resultSet.getString("device_info_id");
		this.resource = resultSet.getString("resource");
		this.status = resultSet.getString("status");
		this.alert = resultSet.getBoolean("alert");
	}

	public DeviceStatusEntity(String deviceInfoId, IDeviceStatus deviceStatus) {
		this.id = UUID.randomUUID().toString();
		this.deviceInfoId = deviceInfoId;

		this.resource = deviceStatus.getResource();
		this.status = deviceStatus.getStatus();
		this.alert = deviceStatus.isAlert();
	}
}
