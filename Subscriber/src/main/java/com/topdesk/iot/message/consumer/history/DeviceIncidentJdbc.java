package com.topdesk.iot.message.consumer.history;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.jdbc.EnhancedSQLException;

public class DeviceIncidentJdbc {

	static final Logger					LOG								= LoggerFactory.getLogger(DeviceIncidentJdbc.class);

	private static final String	DEVICE_INCIDENT_TABLE	= "device_incident";

	private static final String	INSERT_FORMAT			= "insert into " + DEVICE_INCIDENT_TABLE + " (id, incident_id, incident_number) values ('%s', '%s', '%s')";
	private static final String	FETCH_FORMAT			= "select id, incident_id, incident_number from " + DEVICE_INCIDENT_TABLE + " where id = '%s'";
	private static final String	LIST_QUERY				= "select id, incident_id, incident_number from " + DEVICE_INCIDENT_TABLE;
	private static final String DELETE_FORMAT 		= "delete from " + DEVICE_INCIDENT_TABLE + " where id = '%s'";

	private static final String	OBJECT_ID_QUERY_FORMAT	=
		"select " +
		"  a.id, a.incident_id, a.incident_number " +
		"from " +
		"  device_incident as a," +
		"  device_info as b " +
		"where " +
		"  b.device_incident_id = a.id " +
		"  and " +
		"  b.object_id = '%s' ";

	private static final String	OBJECT_ID_RESOURCE_QUERY_FORMAT	=
		"select " +
		"  a.id, a.incident_id, a.incident_number " +
		"from " +
		"  device_incident as a, " +
		"  device_info as b " +
		"where" +
		"  b.device_incident_id = a.id " +
		"  and " +
		"  b.object_id = '%s' " +
		"  and " +
		"  0 < ( select " +
		"          count(*) " +
		"        from " +
		"          device_status as c " +
		"        where " +
		"          c.device_info_id = b.id " +
		"          and " +
		"          c.resource = '%s' )";

	public static void insert(Connection connection, DeviceIncidentEntity deviceIncident) throws EnhancedSQLException {
		String sql = String.format(INSERT_FORMAT, deviceIncident.getId(), deviceIncident.getIncidentUuid(), deviceIncident.getIncidentNumber());
		try {
			boolean autoCommit = connection.getAutoCommit();
			connection.setAutoCommit(false);
			try {
				try (Statement statement = connection.createStatement()) {
					statement.executeUpdate(sql);
				}
				DeviceInfoEntity deviceInfo = deviceIncident.getDeviceInfo();
				DeviceInfoJdbc.insert(connection, deviceInfo);
			} finally {
				connection.setAutoCommit(autoCommit);
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}
	}

	public static DeviceIncidentEntity fetch(Connection connection, String deviceIncidentId) throws EnhancedSQLException, DeviceIncidentException {
		String sql = String.format(FETCH_FORMAT, deviceIncidentId);
		DeviceIncidentEntity deviceIncident = null;
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				deviceIncident = new DeviceIncidentEntity(resultSet);
				deepFetch(connection, deviceIncident);
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}

		return deviceIncident;
	}

	public static Collection<DeviceIncidentEntity> queryList(Connection connection) throws EnhancedSQLException, DeviceIncidentException {
		return queryHelper(connection, LIST_QUERY);
	}

	public static Collection<DeviceIncidentEntity> queryObjectId(Connection connection, String objectId) throws EnhancedSQLException, DeviceIncidentException {
		String sql = String.format(OBJECT_ID_QUERY_FORMAT, objectId);
		return queryHelper(connection, sql);
	}

	public static Collection<DeviceIncidentEntity> queryObjectIdResource(Connection connection, String objectId, String resource) throws EnhancedSQLException, DeviceIncidentException {
		String sql = String.format(OBJECT_ID_RESOURCE_QUERY_FORMAT, objectId, resource);
		return queryHelper(connection, sql);
	}

	private static Collection<DeviceIncidentEntity> queryHelper(Connection connection, String sql) throws EnhancedSQLException, DeviceIncidentException {
		Collection<DeviceIncidentEntity> results = new ArrayList<>();
		try {
			try (Statement statement = connection.createStatement()) {
				ResultSet resultSet = statement.executeQuery(sql);
				while (resultSet.next()) {
					DeviceIncidentEntity deviceIncident = new DeviceIncidentEntity(resultSet);
					deepFetch(connection, deviceIncident);
					results.add(deviceIncident);
				}
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}

		return results;
	}

	/**
	 * @param connection
	 * @param deviceIncidentUuid
	 * @return the number of rows deleted
	 * @throws EnhancedSQLException
	 */
	public static int delete(Connection connection, String deviceIncidentUuid) throws EnhancedSQLException {
		String sql = String.format(DELETE_FORMAT,deviceIncidentUuid);
		int count;
		try (Statement statement = connection.createStatement()) {
			count = statement.executeUpdate(sql);
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}

		return count;
	}

	private static void deepFetch(Connection connection, DeviceIncidentEntity deviceIncident) throws EnhancedSQLException, DeviceIncidentException {
		DeviceInfoEntity deviceInfo = DeviceInfoJdbc.fetchForDeviceIncident(connection,deviceIncident.getId());
		deviceIncident.setDeviceInfo(deviceInfo);
	}

}
