package com.topdesk.iot.message.consumer.history;

public class DeviceIncidentException extends Exception {

	private static final long serialVersionUID = -2755957896628194015L;

	public DeviceIncidentException(String message) {
    super(message);
}

}
