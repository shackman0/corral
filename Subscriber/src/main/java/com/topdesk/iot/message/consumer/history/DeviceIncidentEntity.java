package com.topdesk.iot.message.consumer.history;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeviceIncidentEntity {

	private String        		id;
	private String						incidentUuid;
	private String						incidentNumber;
	private DeviceInfoEntity	deviceInfo;

	public DeviceIncidentEntity(ResultSet resultSet) throws SQLException {
		this.id = resultSet.getString("id");
		this.incidentUuid = resultSet.getString("incident_id");
		this.incidentNumber = resultSet.getString("incident_number");
	}

}
