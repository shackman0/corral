package com.topdesk.iot.message.consumer;

import javax.jms.Connection;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.network.NetworkConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.util.LogMessage;


/**
 * <p>
 * Convenience class to create a subscriber.
 * <p>
 * usage:
 *  MessageListener messageListener = new MyLocalMessageListenerThatDoesSomethingUseful();
 * 	subscriber = new Subscriber(messageListener);
 *	subscriber.start();
 * 	... MyLocalMessageListenerThatDoesSomethingUseful#onMessage invocations ...
 * 	subscriber.stop();
 */
public class Subscriber {

  static final Logger LOG = LoggerFactory.getLogger(Subscriber.class);

  BrokerService embeddedBroker;
	Connection connection;
  Session session;
  ActiveMQConnectionFactory connectionFactory;
  Topic topic;
  MessageConsumer subscriber;
  MessageListener messageListener;


	/**
	 *
	 * @param messageListener callback for received messages
	 */
	public Subscriber (MessageListener messageListener) {
		this.messageListener = messageListener;
	}

	public void start() throws Exception {
		startEmbeddedBroker();
		subscribe2Topic();
	}

	public void stop() throws Exception {
		if (subscriber != null) {
			subscriber.close();
			subscriber = null;
		}

		if (session != null) {
			session.close();
			session = null;
		}

		if (connection != null) {
			connection.close();
			connection = null;
		}

		if (embeddedBroker != null) {
			embeddedBroker.stop();
			embeddedBroker.waitUntilStopped();
			embeddedBroker = null;
		}
  }

	void startEmbeddedBroker() throws Exception {
		embeddedBroker = new BrokerService();
		embeddedBroker.setBrokerName(Configuration.EMBEDDED_BROKER_NAME.value());
		NetworkConnector networkConnector = embeddedBroker.addNetworkConnector(Configuration.EMBEDDED_BROKER_NETWORK_CONNECTOR_TO_REMOTE_BROKER.value());
		networkConnector.setDuplex(true);
		networkConnector.setNetworkTTL(Integer.valueOf(Configuration.HOP_COUNT.value()).intValue());
		embeddedBroker.setUseJmx(Boolean.valueOf(Configuration.EMBEDDED_BROKER_USE_JMX.value()).booleanValue());
		embeddedBroker.getManagementContext().setConnectorPort(Integer.valueOf(Configuration.EMBEDDED_BROKER_JMX_PORT.value()).intValue());
		embeddedBroker.setPersistent(Boolean.valueOf(Configuration.DURABLE_MESSAGES.value()).booleanValue());
		embeddedBroker.start();
		embeddedBroker.waitUntilStarted();
  }

  void subscribe2Topic() throws Exception {
  	connectionFactory = new ActiveMQConnectionFactory(Configuration.CONNECTION_FACTORY_BROKER_URL.value());
    LOG.info(LogMessage.msg("subscribing at " + connectionFactory.getBrokerURL()));

		connection = connectionFactory.createConnection();
    connection.setClientID(Configuration.CLIENT_ID.value());
    connection.start();

    session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

    Topic topic = session.createTopic(Configuration.TOPIC_NAME.value());
    if (Boolean.valueOf(Configuration.DURABLE_MESSAGES.value()).booleanValue()) {
    	subscriber = session.createDurableSubscriber(topic, Configuration.SUBSCRIBER_SUBSCRIPTION_NAME.value());
    } else {
    	subscriber = session.createConsumer(topic);
    }
    subscriber.setMessageListener(messageListener);
  }

}
