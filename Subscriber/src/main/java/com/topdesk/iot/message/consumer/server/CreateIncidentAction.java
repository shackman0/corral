package com.topdesk.iot.message.consumer.server;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.function.Predicate;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.devices.dto.DeviceInfoDto;
import com.topdesk.iot.message.consumer.CreateIncident;
import com.topdesk.iot.message.consumer.Subscriber;
import com.topdesk.iot.message.consumer.history.DeviceIncidentException;
import com.topdesk.messagerouter.client.IRunnable;
import com.topdesk.messagerouter.client.RunUtil;
import com.topdesk.rest.RestApiException;
import com.topdesk.util.LogMessage;

/**
 * CreateIncidents
 * <p>
 * Listens for IoT device status alerts/messages. For devices having one or more error conditions, creates an incident to have the device serviced for all the error conditions.
 */

public class CreateIncidentAction implements IRunnable {

  static final Logger LOG = LoggerFactory.getLogger(CreateIncidentAction.class);

	Subscriber subscriber;
	CreateIncident createIncident;

	public CreateIncidentAction() {
		// do nothing
	}

	/**
	 * login to the REST API and retrieve a session authentication token
	 * run this service until told to stop
	 * this method blocks and returns only when it's time to stop the service
	 * @throws Exception
	 */
	public void start() throws Exception {
		new RunUtil().run(this);
	}

	@Override
	public void run() {
		try {
			createIncident = new CreateIncident();
			subscriber = new Subscriber(new AsyncMessageListener());
			subscriber.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void quiesce() throws Exception {
		subscriber.stop();
		subscriber = null;
		createIncident = null;
  }

  /**
   * AsyncMessageListener
   * <p>
   * receives messages from the message router and causes processing of the message body
   */
  class AsyncMessageListener implements MessageListener {
		@Override
		public void onMessage(Message message) {
    	try {
    		if (message instanceof TextMessage) {
    			// marshall the json into object form
    			String messageText = ((TextMessage)message).getText();   // json
    			LOG.info(LogMessage.msg(messageText));
    			DeviceInfoDto deviceInfo = DeviceInfoDto.convertJson2DeviceInfoDto(messageText);
  		  	createIncident.submitIncidentIfNeeded(deviceInfo);
    		}
			} catch (JMSException | UnsupportedEncodingException | RestApiException | SQLException | DeviceIncidentException e) {
				e.printStackTrace();
			}
		}
  }

  private static final Predicate<Configuration> EXCLUDE_PUBLISHER_OPTIONS = option -> (option.getKey().contains("publisher") == false);

	private static String usage() {
		StringBuilder usage = new StringBuilder();
		usage.append("usage\n")
			.append("  java [-D options] -jar ")
			.append(CreateIncidentAction.class.getSimpleName())
			.append(".jar\n")
			.append("where -D options and their defaults are:\n");
		Configuration.usage(EXCLUDE_PUBLISHER_OPTIONS)
			.forEach(optionString -> {
					usage.append("  ").append(optionString).append("\n");
				} );

		return usage.toString();
	}

	private static String configuration() {
		StringBuilder configuration = new StringBuilder();
		configuration.append("configuration for this execution is:\n");
		Configuration.configuration(EXCLUDE_PUBLISHER_OPTIONS)
			.forEach(optionString -> {
					configuration.append("  ").append(optionString).append("\n");
				} );

		return configuration.toString();
	}

	private static void checkConfiguration() {
		if (Boolean.valueOf(Configuration.DURABLE_MESSAGES.value())) {
			LOG.warn(LogMessage.msg("Durable subscription is enabled. Be sure to also specify a constant " + Configuration.CLIENT_ID.getKey() + " and " + Configuration.SUBSCRIBER_SUBSCRIPTION_NAME.getKey() + ", otherwise, they will be assigned a random UUID and you won't pick up any missed messages."));
		}
	}

	public static void main(String[] args) throws Exception {
		try {
			LOG.info("starting " + CreateIncidentAction.class.getSimpleName());
			LOG.info(usage());
			LOG.info(configuration());
			checkConfiguration();
			new CreateIncidentAction().start();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
  }

}
