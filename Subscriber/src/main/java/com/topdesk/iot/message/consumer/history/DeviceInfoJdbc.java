package com.topdesk.iot.message.consumer.history;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.jdbc.EnhancedSQLException;

public class DeviceInfoJdbc {

	static final Logger					LOG								= LoggerFactory.getLogger(DeviceInfoJdbc.class);

	private static final String	DEVICE_INFO_TABLE	= "device_info";

	private static final String	INSERT_FORMAT			= "insert into " + DEVICE_INFO_TABLE + " (id, device_incident_id, ignore, host, object_id, branch_office, manufacturer, model) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
	private static final String	FETCH_FORMAT			= "select id, device_incident_id, ignore, host, object_id, branch_office, manufacturer, model from " + DEVICE_INFO_TABLE + " where id = '%s'";
	private static final String	QUERY_FORMAT			= "select id, device_incident_id, ignore, host, object_id, branch_office, manufacturer, model from " + DEVICE_INFO_TABLE + " where device_incident_id = '%s'";

	public static void insert(Connection connection, DeviceInfoEntity deviceInfo) throws EnhancedSQLException {
		String sql = String.format(INSERT_FORMAT, deviceInfo.getId(), deviceInfo.getDeviceIncidentId(), String.valueOf(deviceInfo.isIgnore()), deviceInfo.getHost(), deviceInfo.getObjectId(), deviceInfo.getBranchOffice(), deviceInfo.getManufacturer(), deviceInfo.getModel());
		try {
			boolean autoCommit = connection.getAutoCommit();
			connection.setAutoCommit(false);
			try {
				try (Statement statement = connection.createStatement()) {
					statement.executeUpdate(sql);
				}
				for (DeviceStatusEntity deviceStatus : deviceInfo.getDeviceStatuses()) {
					DeviceStatusJdbc.insert(connection, deviceStatus);
				}
			} finally {
				connection.setAutoCommit(autoCommit);
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}
	}

	public static DeviceInfoEntity fetch(Connection connection, String deviceInfoId) throws EnhancedSQLException, DeviceIncidentException {
		DeviceInfoEntity deviceInfo = null;
		String sql = String.format(FETCH_FORMAT, deviceInfoId);
		try (Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
			ResultSet resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				deviceInfo = new DeviceInfoEntity(resultSet);
				deepFetch(connection, deviceInfo);
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}

		return deviceInfo;
	}

	public static DeviceInfoEntity fetchForDeviceIncident(Connection connection, String deviceIncidentId) throws EnhancedSQLException, DeviceIncidentException {
		DeviceInfoEntity deviceInfo;
		String sql = String.format(QUERY_FORMAT, deviceIncidentId);
		try (Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
			ResultSet resultSet = statement.executeQuery(sql);
			resultSet.last();
			int numberOfRows = resultSet.getRow();
			if (numberOfRows == 1) {
				resultSet.first();
				deviceInfo = new DeviceInfoEntity(resultSet);
				deepFetch(connection, deviceInfo);
			} else {
				String msg = String.format("found %d device infos for device incident %s", numberOfRows, deviceIncidentId);
				throw new DeviceIncidentException(msg);
			}
		} catch (SQLException e) {
			throw EnhancedSQLException.builder(e).sql(sql).build();
		}

		return deviceInfo;
	}

	private static void deepFetch(Connection connection, DeviceInfoEntity deviceInfo) throws EnhancedSQLException, DeviceIncidentException {
		Collection<DeviceStatusEntity> deviceStatuses = DeviceStatusJdbc.fetchForDeviceInfo(connection,deviceInfo.getId());
		deviceInfo.setDeviceStatuses(deviceStatuses);
	}

}
