package com.topdesk.iot.message.consumer.history;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeviceInfoEntity implements IDeviceInfo<DeviceStatusEntity> {

	private String													id;
	private String													deviceIncidentId;

	private boolean													ignore;
	private String													host;
	private String													objectId;
	private String													branchOffice;
	private String													manufacturer;
	private String													model;
	private Collection<DeviceStatusEntity>	deviceStatuses;

	public DeviceInfoEntity(ResultSet resultSet) throws SQLException {
		this.id = resultSet.getString("id");
		this.deviceIncidentId = resultSet.getString("device_incident_id");
		this.ignore = resultSet.getBoolean("ignore");
		this.host = resultSet.getString("host");
		this.objectId = resultSet.getString("object_id");
		this.branchOffice = resultSet.getString("branch_office");
		this.manufacturer = resultSet.getString("manufacturer");
		this.model = resultSet.getString("model");
	}

	public DeviceInfoEntity(String deviceIncidentId, IDeviceInfo<IDeviceStatus> deviceInfo) {
		this.id = UUID.randomUUID().toString();
		this.deviceIncidentId = deviceIncidentId;

		this.ignore = deviceInfo.isIgnore();
		this.host = deviceInfo.getHost();
		this.objectId = deviceInfo.getObjectId();
		this.branchOffice = deviceInfo.getBranchOffice();
		this.manufacturer = deviceInfo.getManufacturer();
		this.model = deviceInfo.getModel();

		this.deviceStatuses = new HashSet<>();
		for (IDeviceStatus deviceStatus: deviceInfo.getDeviceStatuses()) {
			DeviceStatusEntity deviceStatusEntity;
			if (deviceStatus instanceof DeviceStatusEntity) {
				deviceStatusEntity = (DeviceStatusEntity)deviceStatus;
			} else {
				deviceStatusEntity = new DeviceStatusEntity(id, deviceStatus);
			}
			deviceStatuses.add(deviceStatusEntity);
		}
	}

}
