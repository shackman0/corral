package com.topdesk.iot.message.consumer.history.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.UUID;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.message.consumer.history.DeviceStatusEntity;
import com.topdesk.iot.message.consumer.history.DeviceStatusJdbc;

public class DeviceStatusTest {

	private static final String CLEAR_TABLE = "delete from device_status";
	private static final String DROP_TABLE_CONSTRAINT = "alter table device_status drop constraint if exists device_info_id";
	private static final String ADD_TABLE_CONSTRAINT = "alter table device_status add constraint device_info_id foreign key (device_info_id) references device_info (id) on delete cascade ;";

	Connection connection;

	@Before
	public void init() throws SQLException {
		connection = DriverManager.getConnection(Configuration.SUBSCRIBER_JDBC_CONNECTION.value(), Configuration.JDBC_USER.value(), Configuration.JDBC_PASSWORD.value());
		try (Statement statement = connection.createStatement()) {
			statement.execute(DROP_TABLE_CONSTRAINT);
			statement.executeUpdate(CLEAR_TABLE);
		}
	}

	@After
	public void wrapup() throws SQLException {
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(CLEAR_TABLE);
			statement.execute(ADD_TABLE_CONSTRAINT);
		}
		connection.close();
	}

	@Test
	public void insertTest() throws SQLException {
		insert();
	}

	private DeviceStatusEntity insert() throws SQLException {
		DeviceStatusEntity deviceStatus = DeviceStatusEntity
			.builder()
			.id(UUID.randomUUID().toString())
			.deviceInfoId(UUID.randomUUID().toString())
			.resource("black toner cartridge")
			.status("3")
			.alert(true)
			.build();
		DeviceStatusJdbc.insert(connection, deviceStatus);

		return deviceStatus;
	}

	@Test
	public void queryTest() throws SQLException {
		DeviceStatusEntity deviceStatus = insert();
		Collection<DeviceStatusEntity> query = DeviceStatusJdbc.fetchForDeviceInfo(connection, deviceStatus.getDeviceInfoId());
		Assert.assertNotNull(query);
		Assert.assertTrue(query.size() > 0);
	}

	@Test
	public void fetchTest() throws SQLException {
		DeviceStatusEntity deviceStatus = insert();
		DeviceStatusEntity fetch = DeviceStatusJdbc.fetch(connection, deviceStatus.getId());
		Assert.assertNotNull(fetch);
		Assert.assertEquals(fetch.getId(), deviceStatus.getId());
	}

}
