package com.topdesk.iot.message.consumer.history.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.message.consumer.history.DeviceIncidentException;
import com.topdesk.iot.message.consumer.history.DeviceInfoEntity;
import com.topdesk.iot.message.consumer.history.DeviceInfoJdbc;

public class DeviceInfoTest {

	private static final String CLEAR_TABLE = "delete from device_info";
	private static final String DROP_TABLE_CONSTRAINT = "alter table device_info drop constraint if exists device_incident_id";
	private static final String ADD_TABLE_CONSTRAINT = "alter table device_info add constraint device_incident_id foreign key (device_incident_id) references device_incident (id) on delete cascade";

	Connection connection;

	@Before
	public void init() throws SQLException {
		connection = DriverManager.getConnection(Configuration.SUBSCRIBER_JDBC_CONNECTION.value(), Configuration.JDBC_USER.value(), Configuration.JDBC_PASSWORD.value());
		try (Statement statement = connection.createStatement()) {
			statement.execute(DROP_TABLE_CONSTRAINT);
			statement.executeUpdate(CLEAR_TABLE);
		}
	}

	@After
	public void wrapup() throws SQLException {
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(CLEAR_TABLE);
			statement.execute(ADD_TABLE_CONSTRAINT);
		}
		connection.close();
	}

	@Test
	public void insertTest() throws SQLException {
		insert();
	}

	private DeviceInfoEntity insert() throws SQLException {
		DeviceInfoEntity deviceInfo = DeviceInfoEntity
			.builder()
			.id(UUID.randomUUID().toString())
			.deviceIncidentId(UUID.randomUUID().toString())
			.branchOffice("Delft, NL")
			.host("PRN1051")
			.ignore(false)
			.manufacturer("HP")
			.model("LaserJet")
			.objectId("PRN1051")
			.deviceStatuses(new ArrayList<>())
			.build();
		DeviceInfoJdbc.insert(connection, deviceInfo);

		return deviceInfo;
	}

	@Test
	public void queryTest() throws SQLException, DeviceIncidentException {
		DeviceInfoEntity deviceInfo = insert();
		DeviceInfoJdbc.fetchForDeviceIncident(connection, deviceInfo.getDeviceIncidentId());
	}

	@Test
	public void fetchTest() throws SQLException, DeviceIncidentException {
		DeviceInfoEntity deviceInfo = insert();
		DeviceInfoEntity fetch = DeviceInfoJdbc.fetch(connection, deviceInfo.getId());
		Assert.assertNotNull(fetch);
		Assert.assertEquals(fetch.getId(), deviceInfo.getId());
	}

}
