package com.topdesk.iot.message.consumer.history.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.message.consumer.history.DeviceIncidentEntity;
import com.topdesk.iot.message.consumer.history.DeviceIncidentException;
import com.topdesk.iot.message.consumer.history.DeviceIncidentJdbc;
import com.topdesk.iot.message.consumer.history.DeviceInfoEntity;
import com.topdesk.iot.message.consumer.history.DeviceInfoJdbc;
import com.topdesk.iot.message.consumer.history.DeviceStatusEntity;
import com.topdesk.iot.message.consumer.history.DeviceStatusJdbc;

public class DeviceIncidentTest {

	private static final String CLEAR_TABLE = "delete from device_incident";

	Connection connection;

	@Before
	public void init() throws SQLException {
		connection = DriverManager.getConnection(Configuration.SUBSCRIBER_JDBC_CONNECTION.value(), Configuration.JDBC_USER.value(), Configuration.JDBC_PASSWORD.value());
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(CLEAR_TABLE);
		}
	}

	@After
	public void wrapup() throws SQLException {
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(CLEAR_TABLE);
		}
		connection.close();
	}

	@Test
	public void insertTest() throws SQLException {
		insert0();
	}

	private DeviceIncidentEntity insert0() throws SQLException {
		DeviceIncidentEntity deviceIncident = DeviceIncidentEntity
			.builder()
			.id(UUID.randomUUID().toString())
			.incidentUuid(UUID.randomUUID().toString())
			.incidentNumber("I 123 45678")
			.build();
		DeviceInfoEntity deviceInfo = DeviceInfoEntity
				.builder()
				.id(UUID.randomUUID().toString())
				.deviceIncidentId(deviceIncident.getId())
				.branchOffice("Delft, NL")
				.host("PRN1051")
				.ignore(false)
				.manufacturer("HP")
				.model("LaserJet")
				.objectId("PRN1051")
				.deviceStatuses(new ArrayList<>())
				.build();
		for (int i = 0; i < 3; i++) {
			DeviceStatusEntity deviceStatus = DeviceStatusEntity
				.builder()
				.id(UUID.randomUUID().toString())
				.alert(i == 0)
				.deviceInfoId(deviceInfo.getId())
				.resource("Black Toner Cartridge #" + i)
				.status(String.valueOf(i*20))
				.build();
			deviceInfo.getDeviceStatuses().add(deviceStatus);
		}
		deviceIncident.setDeviceInfo(deviceInfo);
		DeviceIncidentJdbc.insert(connection, deviceIncident);

		return deviceIncident;
	}

	private DeviceIncidentEntity insert1() throws SQLException {
		DeviceIncidentEntity deviceIncident = DeviceIncidentEntity
			.builder()
			.id(UUID.randomUUID().toString())
			.incidentUuid(UUID.randomUUID().toString())
			.incidentNumber("I 987 65432")
			.build();
		DeviceInfoEntity deviceInfoEntity = DeviceInfoEntity
				.builder()
				.id(UUID.randomUUID().toString())
				.deviceIncidentId(deviceIncident.getId())
				.branchOffice("Budapest, HU")
				.host("PRN1052")
				.ignore(false)
				.manufacturer("Brother")
				.model("InkJet")
				.objectId("PRN1052")
				.deviceStatuses(new ArrayList<>())
				.build();
		ArrayList<DeviceStatusEntity> deviceStatuses = new ArrayList<>();
		DeviceStatusEntity deviceStatus = DeviceStatusEntity
			.builder()
			.id(UUID.randomUUID().toString())
			.deviceInfoId(deviceInfoEntity.getId())
			.alert(true)
			.resource("Black Ink")
			.status("0")
			.build();
		deviceStatuses.add(deviceStatus);
		deviceStatus = DeviceStatusEntity
				.builder()
				.id(UUID.randomUUID().toString())
				.deviceInfoId(deviceInfoEntity.getId())
				.alert(false)
				.resource("Blue Ink")
				.status("90")
				.build();
			deviceStatuses.add(deviceStatus);
			deviceStatus = DeviceStatusEntity
					.builder()
					.id(UUID.randomUUID().toString())
					.deviceInfoId(deviceInfoEntity.getId())
					.alert(false)
					.resource("Yellow Ink")
					.status("75")
					.build();
				deviceStatuses.add(deviceStatus);

		deviceIncident.setDeviceInfo(deviceInfoEntity);
		DeviceIncidentJdbc.insert(connection, deviceIncident);

		return deviceIncident;
	}

	@Test
	public void queryListTest() throws SQLException, DeviceIncidentException {
		DeviceIncidentEntity deviceIncident0 = insert0();
		DeviceIncidentEntity deviceIncident1 = insert1();

		Collection<DeviceIncidentEntity> query = DeviceIncidentJdbc.queryList(connection);
		Assert.assertNotNull(query);
		Assert.assertEquals(2, query.size());
		boolean found0 = false;
		boolean found1 = false;
		for (DeviceIncidentEntity deviceIncident: query) {
			found0 = ((deviceIncident.getId().equals(deviceIncident0.getId())) || found0);
			found1 = ((deviceIncident.getId().equals(deviceIncident1.getId())) || found1);
		}
		Assert.assertTrue(found0);
		Assert.assertTrue(found1);

		DeviceIncidentEntity deviceIncident1st = query.iterator().next();
		DeviceInfoEntity deviceInfo = DeviceInfoJdbc.fetchForDeviceIncident(connection, deviceIncident1st.getId());

		Collection<DeviceStatusEntity> deviceStatuses = DeviceStatusJdbc.fetchForDeviceInfo(connection, deviceInfo.getId());
		Assert.assertFalse(deviceStatuses.isEmpty());

		DeviceIncidentEntity deviceIncident2nd = query.iterator().next();
		deviceInfo = DeviceInfoJdbc.fetchForDeviceIncident(connection, deviceIncident2nd.getId());

		deviceStatuses = DeviceStatusJdbc.fetchForDeviceInfo(connection, deviceInfo.getId());
		Assert.assertFalse(deviceStatuses.isEmpty());
	}

	@Test
	public void queryObjectIdTest() throws SQLException, DeviceIncidentException {
		insert0();
		Collection<DeviceIncidentEntity> query = DeviceIncidentJdbc.queryObjectId(connection, "PRN1051");

		DeviceIncidentEntity deviceIncidentEntity = query.iterator().next();
		DeviceInfoEntity deviceInfo = DeviceInfoJdbc.fetchForDeviceIncident(connection, deviceIncidentEntity.getId());
		Assert.assertEquals("PRN1051", deviceInfo.getObjectId());
	}

	@Test
	public void queryObjectIdResourceTest() throws SQLException, DeviceIncidentException {
		insert0();
		Collection<DeviceIncidentEntity> query = DeviceIncidentJdbc.queryObjectIdResource(connection, "PRN1051", "Black Toner Cartridge #1");

		DeviceIncidentEntity deviceIncident = query.iterator().next();
		DeviceInfoEntity deviceInfo = DeviceInfoJdbc.fetchForDeviceIncident(connection, deviceIncident.getId());

		Collection<DeviceStatusEntity> deviceStatuses = DeviceStatusJdbc.fetchForDeviceInfo(connection, deviceInfo.getId());
		Assert.assertEquals(3, deviceStatuses.size());
	}

	@Test
	public void fetchTest() throws SQLException, DeviceIncidentException {
		DeviceIncidentEntity deviceIncident = insert0();
		DeviceIncidentEntity fetch = DeviceIncidentJdbc.fetch(connection, deviceIncident.getId());
		Assert.assertNotNull(fetch);
		Assert.assertEquals(fetch.getId(), deviceIncident.getId());
	}

	@Test
	public void deleteTest() throws SQLException {
		DeviceIncidentEntity deviceIncident = insert0();
		int count = DeviceIncidentJdbc.delete(connection, deviceIncident.getId());
		Assert.assertTrue(count == 1);
	}

}
