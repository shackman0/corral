package com.topdesk.iot.devices.printers.hpdesignjet;

import java.util.Collection;
import java.util.HashSet;

import org.apache.http.HttpStatus;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.topdesk.iot.devices.AbstractDevice;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;

public class HPDesignJetClient extends AbstractDevice {

	// web pages
//	static String								HOME						= "#";
	static String								INK_LEVELS			= "#hId-consumablePage";
//	static String								INFO						= "#hId-devInfoPage";
//	static String								PAPER						= "#hId-pgPaperSources";

	public HPDesignJetClient() {
		super();
	}

	@Override
	protected String getStatusPagePath() {
		return INK_LEVELS;
	}


	@Override
	protected boolean httpResponseCodeIsOK(int httpResponseCode) {
		return ((httpResponseCode == HttpStatus.SC_OK) || (httpResponseCode == 499));
	}

	@Override
	protected Collection<IDeviceStatus> extractDeviceStatuses(Document dom) {
		Collection<IDeviceStatus> deviceStatuses = new HashSet<>();
		Elements inkLevelElements = dom.getElementsByClass("ink-inkLevel");
		for (Element inkLevelElement: inkLevelElements) {
			Elements inkLevelGraphElements = inkLevelElement.children();
			for (Element inkLevelGraphElement: inkLevelGraphElements) {
				String inkLevelGraphClassName = inkLevelGraphElement.className();
				if (inkLevelGraphClassName.matches("^ilg-.-..pc")) {
					String inkLevelPercent = inkLevelGraphClassName.substring(6,8);
					deviceStatuses.add(new DeviceStatus(inkLevelGraphClassName, inkLevelPercent));
				}
			}
		}

		return deviceStatuses;
	}

	private static final String MANUFACTURER = "Hewlett Packard";
	private static final String MODEL = "Designjet T120";

	@Override
	public boolean canQueryThisDevice(IDeviceInfo<IDeviceStatus> deviceInfo) {
		return ((deviceInfo.getManufacturer().equals(MANUFACTURER)) && (deviceInfo.getModel().equals(MODEL)));
	}

}
