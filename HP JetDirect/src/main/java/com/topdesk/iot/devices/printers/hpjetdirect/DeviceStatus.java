package com.topdesk.iot.devices.printers.hpjetdirect;

import com.topdesk.iot.devices.AbstractDeviceStatus;

public class DeviceStatus extends AbstractDeviceStatus {

	/**
	 * for json marshalling use
	 */
	@SuppressWarnings("unused")
	private DeviceStatus() {
		// do nothing
	}

	public DeviceStatus(String resource, String status) {
		super(resource, status);
		int levelPercent = Integer.valueOf(getStatus());
		this.alert = levelPercent <= 10;
	}

}
