package com.topdesk.iot.devices.printers.hpjetdirect;

import java.util.Collection;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.topdesk.iot.devices.AbstractDevice;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;

import lombok.Getter;
import lombok.Setter;


/**
 * HPJetDirectClient
 * <p>
 *
 *
 */
public class HPJetDirectClient extends AbstractDevice {


	public static final String CSV_FILE_NAME = "HP JetDirect.csv";
	public static final String CSV_FILE_PATH = String.format("D:/repositories/IoT/HP JetDirect/src/main/resources/%s", CSV_FILE_NAME);

	/**
	 * this is an arbitrary upper limit for use by child classes to prevent an infinite loop when looking for statuses
	 */
	@Getter
	@Setter
	private int maximumNumberOfStatuses = 5;

	// web pages
	static String	DEVICE_STATUS					= "hp/device/DeviceStatus/Index";
//	static String	CONFIGURATION				= "hp/device/InternalPages/Index?id=ConfigurationPage";
//	static String	SUPPLIES_STATUS			= "hp/device/InternalPages/Index?id=SuppliesStatus";
//	static String	EVENT_LOG						= "hp/device/InternalPages/Index?id=EventLogPage";
//	static String	USAGE								= "hp/device/InternalPages/Index?id=UsagePage";
//	static String	DEVICE_INFORMATION	= "hp/device/DeviceInformation/View";
//	static String	CONTROL_PANEL				= "hp/device/ControlPanelSnapshot/Index";

	// event log
//	static String MISFEED_JAM_TRAY1 = "13.B2.D1";
//	static String MISFEED_JAM_TRAY2 = "13.A2.D2";
//	static String CARTRIDGE_LOW     = "10.00.60";

	public HPJetDirectClient() {
		super();
	}

	@Override
	protected String getStatusPagePath() {
		return DEVICE_STATUS;
	}

	@Override
	protected Collection<IDeviceStatus> extractDeviceStatuses(Document dom) {
		Collection<IDeviceStatus> deviceStatuses = new HashSet<>();
		int i = 0;
		do {
			Element supplyNameElement = dom.getElementById("SupplyName" + i);
			if (supplyNameElement == null) {
				break;
			}
			String supplyName = supplyNameElement.text();
			Element supplyGaugeElement = dom.getElementById("SupplyGauge" + i);
			String supplyGauge = supplyGaugeElement.text();  // usually expressed in terms of xx%
			String supplyValue = supplyGauge.replaceAll("[^0-9.]", "");  // strip off the trailing % sign
			deviceStatuses.add(new DeviceStatus(supplyName, supplyValue));
			i++;
		} while (i < maximumNumberOfStatuses);

		return deviceStatuses;
	}

	private static final String MANUFACTURER = "Hewlett Packard";
	private static final String MODEL = "LaserJet 602DN";

	@Override
	public boolean canQueryThisDevice(IDeviceInfo<IDeviceStatus> deviceInfo) {
		return ((deviceInfo.getManufacturer().equals(MANUFACTURER)) && (deviceInfo.getModel().equals(MODEL)));
	}

}
