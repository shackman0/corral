package com.topdesk.iot.devices.printers.brotherinkjet;

import java.util.Collection;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.topdesk.iot.devices.AbstractDevice;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;

public class BrotherInkJetClient extends AbstractDevice {

	// web pages
	static String	STATUS_PAGE	= "general/status.html";

	static String STATUS_OK = "tonerremain";

	public BrotherInkJetClient() {
		super();
	}

	@Override
	protected String getStatusPagePath() {
		return STATUS_PAGE;
	}

	/**
	 * we check the following statuses:
	 * 1. black ink
	 * 2. cyan ink
	 * 3. magenta ink
	 * 4. yellow ink
	 */
	@Override
	protected Collection<IDeviceStatus> extractDeviceStatuses(Document dom) {
		Collection<IDeviceStatus> deviceStatuses = new HashSet<>();

		deviceStatuses.add(extractDeviceStatus("Black", dom));
		deviceStatuses.add(extractDeviceStatus("Cyan", dom));
		deviceStatuses.add(extractDeviceStatus("Magenta", dom));
		deviceStatuses.add(extractDeviceStatus("Yellow", dom));

		return deviceStatuses;
	}

	IDeviceStatus extractDeviceStatus(String altText, Document dom) {
		String cssSelector = String.format("img[alt^=\"%s\"]", altText);
		Elements elementsMatchingOwnText = dom.select(cssSelector);
		Element matchingElement = elementsMatchingOwnText.first();
		String status = matchingElement.className();
		return new DeviceStatus(altText, status);
	}

	@Override
	protected String getProtocol() {
		return HTTP;
	}

	private static final String MANUFACTURER = "Brother";
	private static final String MODEL = "DCP-9020CDW";

	@Override
	public boolean canQueryThisDevice(IDeviceInfo<IDeviceStatus> deviceInfo) {
		return ((deviceInfo.getManufacturer().equals(MANUFACTURER)) && (deviceInfo.getModel().equals(MODEL)));
	}

}
