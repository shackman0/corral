package com.topdesk.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * Carrier
 * <p>
 * carries any arbitrary payload. used to pass Input/Output parameters.
 *
 * @param <T>
 */
@NoArgsConstructor
@AllArgsConstructor
public class OutParameter<T> {

	@Getter
	@Setter
	T payload;

	public void clearPayload() {
		payload = null;
	}

}
