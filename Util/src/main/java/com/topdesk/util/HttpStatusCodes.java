package com.topdesk.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lombok.Getter;

public class HttpStatusCodes {

	static final String FILE_NAME = "http-status-codes.xml";
	static final String FOLDER_PATH = "D:/repositories/IoT/util/src/main/resources/";

	@Getter
	static Map<Integer, String> statusCodeDescriptions = getStatusCodes();

	public static Map<Integer, String> getStatusCodes() {
		if (statusCodeDescriptions == null) {
			statusCodeDescriptions = new DefaultHashMap<>("Unassigned");
			File file = new File(FOLDER_PATH + FILE_NAME);
			try {
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    InputSource inputSource = new InputSource(new FileReader(file));
		    Document doc = db.parse(inputSource);
		    NodeList recordNodes = doc.getElementsByTagName("record");
		    for(int i = 0; i < recordNodes.getLength(); i++) {
		    	Element recordElement = (Element) recordNodes.item(i);
		    	Element descriptionElement = (Element)recordElement.getElementsByTagName("description").item(0);
		    	String description = descriptionElement.getTextContent();
		    	if (description.equalsIgnoreCase("unassigned") == false) {
		    		Element valueElement = (Element) recordElement.getElementsByTagName("value").item(0);
		    		String[] values = valueElement.getTextContent().split("-");
		    		Integer lowerValue = Integer.valueOf(values[0]);
		    		Integer upperValue = Integer.valueOf(values.length == 1 ? values[0] : values[1]);
		    		for (int j = lowerValue; j <= upperValue; j++) {
		    			statusCodeDescriptions.put(j, description);
		    		}
		    	}
		    }
			} catch (ParserConfigurationException | SAXException | IOException e) {
				throw new RuntimeException(e);
			}
		}
		return statusCodeDescriptions;
	}

	public static String getStatusCodeDescription(int statusCode) {
		return statusCodeDescriptions.get(statusCode);
	}

}
