package com.topdesk.util;

/**
 * LogMessage
 * <p>
 * usage
 *   Logger logger = LoggerFactory.getLogger(LogMessage.class);
 *   logger.info(LogMessage.msg("test"));
 *
 */

public class LogMessage {

	private LogMessage() {
		// do nothing
	}

	public static String msg(String message) {
		StackTraceElement callerStackTraceElement = Thread.currentThread().getStackTrace()[2];
		String logMessage = buildMessage(message, callerStackTraceElement);

	  return logMessage;
	}

	private static String buildMessage(String message, StackTraceElement callerStackTraceElement) {
		StringBuilder builder = new StringBuilder();
		String[] className = callerStackTraceElement.getClassName().split("\\.");
		builder.append(className[className.length - 1]);
		builder.append("#");
		builder.append(callerStackTraceElement.getMethodName());
		builder.append("@");
		builder.append(callerStackTraceElement.getLineNumber());
		builder.append(": ");

		builder.append(message);

		return builder.toString();
	}

//	public static void main (String... strings) {
//		Logger logger = LoggerFactory.getLogger(LogMessage.class);
//	  logger.info("test");
//	  logger.info(LogMessage.msg("test"));
//	}
}
