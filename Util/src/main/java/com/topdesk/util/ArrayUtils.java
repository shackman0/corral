package com.topdesk.util;

public class ArrayUtils {

	/**
	 * all static
	 */
	private ArrayUtils() {
		// do nothing
	}

	@SafeVarargs
	public static <T> T[] append(T[] array, T... appendElements) {
		return concat(array, appendElements);
	}

	@SafeVarargs
	public static <T> T[] prepend(T[] array, T... prependElements) {
		return concat(prependElements, array);
	}

	public static <T> T[] concat(T[] array1, T[] array2) {
		T[] newArray = java.util.Arrays.copyOf(array1, array1.length + array2.length);
		System.arraycopy(array2, 0, newArray, array1.length, array2.length);

		return newArray;
	}

}
