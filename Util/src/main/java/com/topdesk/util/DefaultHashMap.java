package com.topdesk.util;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

public class DefaultHashMap<K,V> extends HashMap<K,V> {

	private static final long serialVersionUID = 3070029443792951097L;

	@Getter
	@Setter
	protected V defaultValue = null;

  public DefaultHashMap() {
  	// no default
  }

  public DefaultHashMap(V defaultValue) {
    this.defaultValue = defaultValue;
  }

  @Override
  public V get(Object k) {
    return containsKey(k) ? super.get(k) : defaultValue;
  }

}