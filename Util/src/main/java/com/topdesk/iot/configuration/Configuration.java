package com.topdesk.iot.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

/**
 * Constants
 * <p>
 * system-wide settings.
 *
 * todo: use configuration service
 */
public enum Configuration {
	REMOTE_BROKER_HOST {
		@Override
		public String getKey() { return REMOTE_BROKER_HOST_KEY; }
		@Override
		public String getDefault() { return "localhost";	}
	},
	REMOTE_BROKER_PORT {
		@Override
		public String getKey() { return REMOTE_BROKER_PORT_KEY; }
		@Override
		public String getDefault() { return String.valueOf(61616);	}
	},
	EMBEDDED_BROKER_NAME {
		@Override
		public String getKey() { return EMBEDDED_BROKER_NAME_KEY; }
		String defaultEmbeddedBrokerName = UUID.randomUUID().toString();
		@Override
		public String getDefault() { return defaultEmbeddedBrokerName; }
	},
	EMBEDDED_BROKER_NETWORK_CONNECTOR_TO_REMOTE_BROKER {
		@Override
		public String getKey() { return EMBEDDED_BROKER_NETWORK_CONNECTOR_TO_REMOTE_BROKER_KEY; }
		@Override
		public String getDefault() { return String.format("static:(tcp://%s:%s)", REMOTE_BROKER_HOST.value(), REMOTE_BROKER_PORT.value()); }
	},
	EMBEDDED_BROKER_JMX_PORT {
		@Override
		public String getKey() { return Configuration.EMBEDDED_BROKER_JMX_PORT_KEY; }
		@Override
		public String getDefault() { return String.valueOf(10990);	}
	},
	TOPIC_NAME {
		@Override
		public String getKey() { return TOPIC_NAME_KEY; }
		@Override
		public String getDefault() { return "IoT Device Alerts";	}
	},
	CLIENT_ID {
		@Override
		public String getKey() { return CLIENT_ID_KEY; }
		String defaultClientId = UUID.randomUUID().toString();
		@Override
		public String getDefault() {
			return defaultClientId;	}
	},
	SUBSCRIBER_SUBSCRIPTION_NAME {
		@Override
		public String getKey() { return SUBSCRIBER_SUBSCRIPTION_NAME_KEY; }
		String defaultSubscriptionName = UUID.randomUUID().toString();
		@Override
		public String getDefault() { return defaultSubscriptionName; }
	},
	DURABLE_MESSAGES {
		@Override
		public String getKey() { return DURABLE_MESSAGES_KEY; }
		@Override
		public String getDefault() { return Boolean.FALSE.toString(); }
	},
	PUBLISHER_TRANSACTION_SIZE {
		@Override
		public String getKey() { return PUBLISHER_TRANSACTION_SIZE_KEY; }
		@Override
		public String getDefault() { return String.valueOf(10); }
	},
	PUBLISHER_DEVICES_FILE_PATH {
		@Override
		public String getKey() { return PUBLISHER_DEVICES_FILE_PATH_KEY; }
		@Override
		public String getDefault() { return "./conf/Devices.csv";	}
	},
	PUBLISHER_POLL_INTERVAL {
		@Override
		public String getKey() { return PUBLISHER_POLL_INTERVAL_KEY; }
		@Override
		public String getDefault() { return String.valueOf(30); }   // minutes
	},
	EMBEDDED_BROKER_USE_JMX {
		@Override
		public String getKey() { return EMBEDDED_BROKER_USE_JMX_KEY; }
		@Override
		public String getDefault() { return Boolean.FALSE.toString();	}
	},
	HOP_COUNT {
		@Override
		public String getKey() { return HOP_COUNT_KEY; }
		@Override
		public String getDefault() {
			return String.valueOf(10); }
	},
	PUBLISHER_JDBC_CONNECTION {
		@Override
		public String getKey() { return PUBLISHER_JDBC_CONNECTION_KEY; }
		@Override
		public String getDefault() { return "jdbc:postgresql://PC1795:5432/corral_???";	}   // ?ssl=true
	},
	SUBSCRIBER_JDBC_CONNECTION {
		@Override
		public String getKey() { return SUBSCRIBER_JDBC_CONNECTION_KEY; }
		@Override
		public String getDefault() { return "jdbc:postgresql://PC1795:5432/corral_device_incident";	}   // ?ssl=true
	},
	CONNECTION_FACTORY_BROKER_URL {
		@Override
		public String getKey() { return CONNECTION_FACTORY_BROKER_URL_KEY; }
		@Override
		public String getDefault() { return String.format(CONNECTION_FACTORY_BROKER_URL_FORMAT, REMOTE_BROKER_HOST.getDefault(), REMOTE_BROKER_PORT.getDefault(), EMBEDDED_BROKER_NAME.getDefault());	}
		@Override
		public String value() { return String.format(CONNECTION_FACTORY_BROKER_URL_FORMAT, REMOTE_BROKER_HOST.value(), REMOTE_BROKER_PORT.value(), EMBEDDED_BROKER_NAME.value());	}
	},
	SUBSCRIBER_TOPDESK_API_ROOT_URL {
		@Override
		public String getKey() { return SUBSCRIBER_TOPDESK_API_ROOT_URL_KEY; }
		@Override
		public String getDefault() { return "https://toperations-acc.topdesk.com/tas/api";	}     // "http://sneaks.topdesk.com:751/tas/api"
	},
	SUBSCRIBER_TOPDESK_API_AUTHENTICATION_USERID {
		@Override
		public String getKey() { return Configuration.SUBSCRIBER_TOPDESK_API_AUTHENTICATION_USERID_KEY; }
		@Override
		public String getDefault() { return "PRINTERMONITOR";	}
	},
	SUBSCRIBER_TOPDESK_API_AUTHENTICATION_PASSWORD {
		@Override
		public String getKey() { return SUBSCRIBER_TOPDESK_API_AUTHENTICATION_PASSWORD_KEY; }
		@Override
		public String getDefault() { return "PRINTERMONITOR";	}
	},
	JDBC_USER {
		@Override
		public String getKey() { return JDBC_USER_KEY; }
		@Override
		public String getDefault() { return "corral";	}
	},
	JDBC_PASSWORD {
		@Override
		public String getKey() { return JDBC_PASSWORD_KEY; }
		@Override
		public String getDefault() { return "corral";	}
	},
	INCIDENT_CATEGORY {
		@Override
		public String getKey() { return INCIDENT_CATEGORY_KEY; }
		@Override
		public String getDefault() { return "ICT";	}
	},
	INCIDENT_SUB_CATEGORY {
		@Override
		public String getKey() { return INCIDENT_SUB_CATEGORY_KEY; }
		@Override
		public String getDefault() { return "Printers";	}
	},
	INCIDENT_OPERATOR_SURNAME {
		@Override
		public String getKey() { return INCIDENT_OPERATOR_SURNAME_KEY; }
		@Override
		public String getDefault() { return "ICT Delft";	}
	},
	INCIDENT_OPERATOR_FIRSTNAME {
		@Override
		public String getKey() { return INCIDENT_OPERATOR_FIRSTNAME_KEY; }
		@Override
		public String getDefault() { return "";	}
	},
	INCIDENT_OPERATOR_GROUP {
		@Override
		public String getKey() { return INCIDENT_OPERATOR_GROUP_KEY; }
		@Override
		public String getDefault() { return "ICT Delft";	}
	},
	INCIDENT_PERSON_SURNAME {
		@Override
		public String getKey() { return INCIDENT_PERSON_SURNAME_KEY; }
		@Override
		public String getDefault() { return "ICT Delft";	}
	},
	INCIDENT_PERSON_FIRSTNAME {
		@Override
		public String getKey() { return INCIDENT_PERSON_FIRSTNAME_KEY; }
		@Override
		public String getDefault() { return "";	}
	},
	INCIDENT_URGENCY {
		@Override
		public String getKey() { return INCIDENT_URGENCY_KEY; }
		@Override
		public String getDefault() { return "Normal";	}
	};

	public abstract String getKey();
	public abstract String getDefault();

	/**
	 * @return the current value for the configuration setting
	 */
	public String value() {
		return System.getProperty(getKey(), getDefault());
	}

	/**
	 * @return key="default value"
	 */
	public String getKeyDefault() {
		return String.format("%s=\"%s\"", getKey(), getDefault());
	}

	/**
	 * @return key="actual value"
	 */
	public String getKeyValue() {
		return String.format("%s=\"%s\"", getKey(), value());
	}

	/**
	 * @param predicate use this to filter on the configurations that are displayed
	 * @return human readable sorted list of keys and their default values
	 */
	public static List<String> usage(Predicate<Configuration> predicate) {
		ArrayList<String> usage = new ArrayList<>();
		Arrays.stream(Configuration.values())
			.sorted((config0, config1) -> config0.getKey().compareToIgnoreCase(config1.getKey()))
			.filter(option -> predicate.test(option))
			.forEach(option -> usage.add(option.getKeyDefault()));

		return usage;
	}

	/**
	 * @param predicate
	 * @return human readable sorted list of keys and their current run-time values
	 */
	public static List<String> configuration(Predicate<Configuration> predicate) {
		ArrayList<String> usage = new ArrayList<>();
		Arrays.stream(Configuration.values())
			.sorted((config0, config1) -> config0.getKey().compareToIgnoreCase(config1.getKey()))
			.filter(option -> predicate.test(option))
			.forEach(option -> usage.add(option.getKeyValue()));

		return usage;
	}

	static final String REMOTE_BROKER_HOST_KEY = "com.topdesk.remotebroker.host";
	static final String REMOTE_BROKER_PORT_KEY = "com.topdesk.remotebroker.port";

	static final String EMBEDDED_BROKER_NAME_KEY = "com.topdesk.embeddedbroker.name";
	static final String EMBEDDED_BROKER_NETWORK_CONNECTOR_TO_REMOTE_BROKER_KEY = "com.topdesk.embeddedbroker.networkconnector2remotebroker";
	static final String EMBEDDED_BROKER_JMX_PORT_KEY = "com.topdesk.embeddedbroker.jmx.port";
	static final String EMBEDDED_BROKER_USE_JMX_KEY = "com.topdesk.iot.embeddedbroker.usejmx";

	static final String CONNECTION_FACTORY_BROKER_URL_KEY = "com.topdesk.connectionfactory.brokerurl";
	static final String CONNECTION_FACTORY_BROKER_URL_FORMAT = "failover:(tcp://%s:%s,vm://%s)?randomize=false";

	static final String HOP_COUNT_KEY = "com.topdesk.iot.hopcount";
	static final String DURABLE_MESSAGES_KEY = "com.topdesk.iot.messaging.durable";

	static final String TOPIC_NAME_KEY = "com.topdesk.iot.topicname";
	static final String CLIENT_ID_KEY = "com.topdesk.iot.messaging.clientid";

	static final String PUBLISHER_JDBC_CONNECTION_KEY = "com.topdesk.iot.publisher.jdbcconnection";
	static final String SUBSCRIBER_JDBC_CONNECTION_KEY = "com.topdesk.iot.subscriber.jdbcconnection";
	static final String JDBC_USER_KEY = "com.topdesk.iot.jdbcuser";
	static final String JDBC_PASSWORD_KEY = "com.topdesk.iot.jdbcpassword";

	static final String PUBLISHER_TRANSACTION_SIZE_KEY = "com.topdesk.iot.publisher.transactionsize";
	static final String PUBLISHER_POLL_INTERVAL_KEY = "com.topdesk.iot.publisher.pollinterval";
	static final String PUBLISHER_DEVICES_FILE_PATH_KEY = "com.topdesk.iot.publisher.devices.filepath";

	static final String SUBSCRIBER_SUBSCRIPTION_NAME_KEY = "com.topdesk.iot.subscriber.subscriptionname";
	static final String SUBSCRIBER_TOPDESK_API_ROOT_URL_KEY = "com.topdesk.iot.subscriber.topdeskapi.rooturl";
	static final String SUBSCRIBER_TOPDESK_API_AUTHENTICATION_USERID_KEY = "com.topdesk.iot.subscriber.topdeskapi.authentication.userid";
	static final String SUBSCRIBER_TOPDESK_API_AUTHENTICATION_PASSWORD_KEY = "com.topdesk.iot.subscriber.topdeskapi.authentication.password";

	static final String INCIDENT_CATEGORY_KEY = "com.topdesk.iot.topdeskapi.incident.category";
	static final String INCIDENT_SUB_CATEGORY_KEY = "com.topdesk.iot.topdeskapi.incident.subcategory";
	static final String INCIDENT_OPERATOR_SURNAME_KEY = "com.topdesk.iot.topdeskapi.incident.operator.surname";
	static final String INCIDENT_OPERATOR_FIRSTNAME_KEY = "com.topdesk.iot.topdeskapi.incident.operator.firstname";
	static final String INCIDENT_OPERATOR_GROUP_KEY = "com.topdesk.iot.topdeskapi.incident.operatorgroup";
	static final String INCIDENT_PERSON_SURNAME_KEY = "com.topdesk.iot.topdeskapi.incident.person.surname";
	static final String INCIDENT_PERSON_FIRSTNAME_KEY = "com.topdesk.iot.topdeskapi.incident.person.firstname";
	static final String INCIDENT_URGENCY_KEY = "com.topdesk.iot.topdeskapi.incident.urgency";
}
