package com.topdesk.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgreSQL {

	static final String CONNECTION_STRING_FORMAT = "jdbc:postgresql://%s:%d/%s";
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static {
		// register the JDBC driver
		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/*
	 * all static
	 */
	private PostgreSQL() {
		// do nothing
	}


	/**
	 * use this in a try with resources block
	 * @param host
	 * @param port
	 * @param databaseName
	 * @param user
	 * @param password
	 * @return
	 * @throws SQLException
	 */
	public static Connection connect(String host, int port, String databaseName, String user, String password) throws SQLException {
		String dbUrl = String.format(CONNECTION_STRING_FORMAT, host, port, databaseName);
		Connection connection = DriverManager.getConnection(dbUrl, user, password);
		return connection;
	}

}
