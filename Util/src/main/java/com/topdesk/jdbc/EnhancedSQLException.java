package com.topdesk.jdbc;

import java.sql.SQLException;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EnhancedSQLException extends SQLException {

	private static final long serialVersionUID = -6521707057069895277L;

	private Integer     returnCode;
	private String      sql;

	public EnhancedSQLException(String message) {
		super(message);
	}

	public EnhancedSQLException(Throwable cause) {
		super(cause);
	}

	public EnhancedSQLException(String message, Throwable cause) {
		super(message, cause);
	}

	@Override
	public String getMessage() {
		StringBuilder fullMessage = new StringBuilder();
		if (super.getMessage() != null) {
			fullMessage.append(super.getMessage());
		}
		if (returnCode != null) {
			fullMessage.append("\n");
			fullMessage.append("Return code: " + returnCode.toString());
		}
		if (sql != null) {
			fullMessage.append("\n");
			fullMessage.append(sql);
		}

		return fullMessage.toString();
	}

	public static Builder builder(String message) {
		return new Builder(message);
	}

	public static Builder builder(Throwable cause) {
		return new Builder(cause);
	}

	public static Builder builder(String message, Throwable cause) {
		return new Builder(message, cause);
	}

	public static class Builder {

		private EnhancedSQLException enhancedSQLException;
		private Integer     returnCode;
		private String      sql;

		public Builder(String message) {
			this.enhancedSQLException = new EnhancedSQLException(message);
		}

		public Builder(Throwable cause) {
			this.enhancedSQLException = new EnhancedSQLException(cause);
		}

		public Builder(String message, Throwable cause) {
			this.enhancedSQLException = new EnhancedSQLException(message, cause);
		}

		public Builder returnCode(Integer returnCode) {
			this.returnCode = returnCode;
			return this;
		}

		public Builder sql(String sql) {
			this.sql = sql;
			return this;
		}

		public EnhancedSQLException build() {
			enhancedSQLException.setReturnCode(returnCode);
			enhancedSQLException.setSql(sql);

			return enhancedSQLException;
		}
	}
}
