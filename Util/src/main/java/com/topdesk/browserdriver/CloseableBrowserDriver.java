package com.topdesk.browserdriver;

import java.io.Closeable;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.xml.sax.SAXException;

import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import com.machinepublishers.jbrowserdriver.RequestHeaders;
import com.machinepublishers.jbrowserdriver.Settings;
import com.machinepublishers.jbrowserdriver.Timezone;
import com.machinepublishers.jbrowserdriver.UserAgent;
import com.topdesk.util.OutParameter;



/**
 * CloseableBrowserDriver
 * <p>
 * example usage:
 * <code>
	try (CloseableBrowserDriver browserDriver = new CloseableBrowserDriver()) {
		Carrier<String> webPage = new Carrier<>();
		int responseCode = browserDriver.browse("https://prn1043/#hId-consumablePage", webPage);
		System.out.println(responseCode + " \n" + webPage.getPayload());
	}
 * </code>
 */

public class CloseableBrowserDriver implements Closeable {

	private Settings settings;
	private JBrowserDriver driver;

	public CloseableBrowserDriver() {
		// do nothing
	}

	/**
	 * if settings is not currently specified, will initialize settings to this default configuration
	 * @return settings
	 */
	public Settings getSettings() {
		if (settings == null) {
			settings = Settings
					.builder()
					.ajaxResourceTimeout(5000)
					.ajaxWait(5000)
					.blockAds(true)
					.headless(true)
					.hostnameVerification(false)
					.ignoreDialogs(true)
					.requestHeaders(RequestHeaders.CHROME)
					.ssl("trustanything")
					.timezone(Timezone.EUROPE_AMSTERDAM)
					.userAgent(UserAgent.CHROME)
					.build();
		}
		return settings;
	}

	public void setSettings(Settings settings) {
		quit();
		this.settings = settings;
	}

	/**
	 * if a driver is not currently specified, will create a driver using the current settings.
	 * @return
	 */
	public JBrowserDriver getDriver() {
		if (driver == null) {
			driver = new JBrowserDriver(getSettings());
		}
		return driver;
	}

	/**
	 * 1. browse to the URL using the current driver
	 * 2. capture the results. dives "deep" into iframes and includes their sources as well.
	 * @param url
	 * @param domCarrier
	 * @return response code
	 * @throws TransformerException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public int browse(String url, OutParameter<Document> domCarrier) throws IOException, TransformerException, SAXException, ParserConfigurationException {
		JBrowserDriver localDriver = getDriver();
		localDriver.get(url);
		Document document = recurseIntoFrames(localDriver);
		int statusCode = localDriver.getStatusCode();
		domCarrier.setPayload(document);

		return statusCode;
	}

	/**
	 * the "top" of the document assembler process.
	 * dives "deep" into frames & iframes and includes their sources as well.
	 * @param browserDriver
	 * @return a Document that contains the entirety of the rendered page
	 * @throws IOException
	 * @throws TransformerException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	Document recurseIntoFrames(JBrowserDriver browserDriver) throws IOException, TransformerException, SAXException, ParserConfigurationException {
		TargetLocator switchTo = browserDriver.switchTo();
		switchTo.defaultContent();    // start at the top
		Document document = Document.createShell("");
		int numberOfChildNodes = document.childNodeSize();
		while (numberOfChildNodes > 0) {
			document.childNode(--numberOfChildNodes).remove();
		}
		recurseIntoFramesHelper(switchTo, browserDriver, document);

		return document;
	}

	/**
	 * dives "deep" into frames & iframes that appear within the parent element and includes their sources as well
	 * @param switchTo
	 * @param browserDriver
	 * @param parent
	 * @throws IOException
	 * @throws TransformerException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	void recurseIntoFramesHelper(TargetLocator switchTo, JBrowserDriver browserDriver, Element parent) throws IOException, TransformerException, SAXException, ParserConfigurationException {
		String pageSource = browserDriver.getPageSource();
		Document document = Jsoup.parse(pageSource);
		parent.appendChild(document);
		Elements frameElements = new Elements();
		frameElements.addAll(document.getElementsByTag("frame"));
		frameElements.addAll(document.getElementsByTag("iframe"));
		for (int i = 0; i < frameElements.size(); i++) {
			Element frameElement = frameElements.get(i);
			switchTo.frame(i);
			recurseIntoFramesHelper(switchTo, browserDriver, frameElement);
			switchTo.parentFrame();
		}
	}

	/**
	 * you must call quit() when you are done, otherwise the engine process will still be running on your system.
	 * it is recommended that you use CloseableBrowserDriver in a try-with-resource block so that quit() gets called automatically for you.
	 */
	public void quit() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
		settings = null;
	}

	/**
	 * it is recommended that you use CloseableBrowserDriver in a try-with-resource block so that quit() gets called automatically for you.
	 */
	@Override
	public void close() {
		quit();
	}

}
