package com.topdesk.browserdriver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;

import com.topdesk.rest.RestApiException;
import com.topdesk.util.OutParameter;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;

/**
 * Client
 * <p>
 * HTTP client conveniences
 */
public class Client {

	public static DateTransformer DATE_TRANSFORMER = new DateTransformer("yyyy-MM-dd'T'HH:mm:ss.SSS");  // TOPdesk API format
	public static JSONSerializer JSON_SERIALIZER = new JSONSerializer()
		.prettyPrint(true)
		.exclude("*.class")
		.transform(DATE_TRANSFORMER, Date.class);

	private static final String USER_AGENT = "Mozilla/5.0";

	/**
	 * everything's static
	 */
	private Client() {
		// do nothing
	}

	public static CloseableHttpClient getCloseableHttpClient() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		TrustStrategy trustStrategy = new TrustSelfSignedStrategy();
		SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(trustStrategy).build();
		HostnameVerifier hostnameVerifier = NoopHostnameVerifier.INSTANCE;
		CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(hostnameVerifier).build();

		return httpClient;
	}

	public static StatusLine httpClientExecute(String authenticationToken, HttpUriRequest httpRequest, OutParameter<String> jsonResponseCarrier, Integer... nonErrorResponseCodes) throws RestApiException {
		setHeaders(authenticationToken, httpRequest);
		StatusLine statusLine = null;
    try (CloseableHttpClient httpClient = getCloseableHttpClient()) {
			try (CloseableHttpResponse httpResponse = httpClient.execute(httpRequest)) {
				statusLine = httpResponse.getStatusLine();
				HttpEntity responseEntity = httpResponse.getEntity();
				StringBuilder responseString = new StringBuilder();
				if (responseEntity == null) {
					jsonResponseCarrier.setPayload(null);
				} else {
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
					String inputLine;
					while ((inputLine = bufferedReader.readLine()) != null) {
						responseString.append(inputLine);
					}
					jsonResponseCarrier.setPayload(responseString.toString());
				}
				if (isHttpError(statusLine.getStatusCode(), nonErrorResponseCodes)) {
					InputStream requestEntityContent = null;
					if (httpRequest instanceof HttpPost) {
						requestEntityContent = ((HttpPost)httpRequest).getEntity().getContent();
					}
					RestApiException restApiException = RestApiException
						.builder()
						.requestLine(httpRequest.getRequestLine())
						.requestEntityContent(requestEntityContent)
						.statusLine(statusLine)
						.responseBody(responseString.toString())
						.headers(httpRequest.getAllHeaders())
						.build();
					throw restApiException;
				}
			}
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			throw new RestApiException(e);
		}

    return statusLine;
	}

	/**
	 * non-error http response codes. generally, you only throw an exception for http errors and not for http infos.
	 */
	static Integer[] HTTP_INFO_CODES = {
		HttpURLConnection.HTTP_ACCEPTED,
		HttpURLConnection.HTTP_CREATED,
		HttpURLConnection.HTTP_NO_CONTENT,
		HttpURLConnection.HTTP_NOT_FOUND,
		HttpURLConnection.HTTP_OK,
		HttpURLConnection.HTTP_PARTIAL
		};
	static List<Integer> HTTP_INFO_CODES_LIST = Arrays.asList(HTTP_INFO_CODES);

	public static boolean isHttpInfo(int httpStatusCode, Integer... nonErrorResponseCodes) {
		List<Integer> nonErrorResponseCodesList;
		if ((nonErrorResponseCodes == null) || (nonErrorResponseCodes.length == 0)) {
			nonErrorResponseCodesList = HTTP_INFO_CODES_LIST;
		} else {
			nonErrorResponseCodesList = Arrays.asList(nonErrorResponseCodes);
		}
		return nonErrorResponseCodesList.contains(httpStatusCode);
	}

	public static boolean isHttpError(int httpStatusCode, Integer... nonErrorResponseCodes) {
		return !isHttpInfo(httpStatusCode, nonErrorResponseCodes);
	}

	static void setHeaders(String authenticationToken, HttpUriRequest httpRequest) {
		httpRequest.setHeader("Accept", "application/json");
		httpRequest.setHeader("Content-Type", "application/json");
		httpRequest.setHeader("Authorization", String.format("TOKEN id=\"%s\"", authenticationToken));
		httpRequest.setHeader("User-Agent", USER_AGENT);
	}

}
