package com.topdesk.browserdriver;

public class WebException extends Exception {

	private static final long serialVersionUID = 4658953985801859259L;

	public WebException() {
		super();
	}

	public WebException(String message) {
		super(message);
	}

	public WebException(String message, Throwable cause) {
		super(message, cause);
	}

	public WebException(Throwable cause) {
		super(cause);
	}

}
