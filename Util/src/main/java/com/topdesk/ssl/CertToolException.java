package com.topdesk.ssl;

public class CertToolException extends Exception {

	private static final long serialVersionUID = -2220338505609458181L;

	public CertToolException() {
		super();
	}

	public CertToolException(String message) {
		super(message);
	}

	public CertToolException(String message, Throwable cause) {
		super(message, cause);
	}

	public CertToolException(Throwable cause) {
		super(cause);
	}

}
