package com.topdesk.ssl;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Builder;
import lombok.NonNull;

/**
 * Certificate Tool
 *
 * Add a server's certificate to the java jssecacerts/cacerts KeyStore with your other trusted certificates.
 *
 * http://nodsw.com/blog/leeland/2006/12/06-no-more-unable-find-valid-certification-path-requested-target https://github.com/escline/InstallCert/blob/master/InstallCert.java
 *
 * Use: java CertTool hostname[:port] [passphrase] Example: % java CertTool ecc.fedora.redhat.com
 */

/**
 * CertTool
 * <p>
 *
 *
 */

@Builder
public class CertTool {

	final static Logger LOG = LoggerFactory.getLogger(CertTool.class);

	static final String CACERTS = "cacerts";
	static final String JSSECACERTS = "jssecacerts";
	static final char[] CHANGE_IT = "changeit".toCharArray();

	public enum Mode {
		EMBEDDED
		{
			@Override
			public int chooseCertificate(X509Certificate[] chain) {
				// arbitrarily choose the first one
				return 0;
			}
		},
		COMMAND_LINE
		{
			@Override
			public int chooseCertificate(X509Certificate[] chain) throws NoSuchAlgorithmException, CertificateEncodingException {
				// please choose 1 certificate from the cert chain.
				LOG.info("Server sent " + chain.length + " certificate(s):\n");
				MessageDigest sha1 = MessageDigest.getInstance("SHA1");
				MessageDigest md5 = MessageDigest.getInstance("MD5");
				for (int i = 0; i < chain.length; i++) {
					X509Certificate cert = chain[i];
					LOG.info(" " + (i + 1) + " Subject " + cert.getSubjectDN());
					LOG.info("   Issuer  " + cert.getIssuerDN());
					sha1.update(cert.getEncoded());
					LOG.info("   sha1    " + toHexString(sha1.digest()));
					md5.update(cert.getEncoded());
					LOG.info("   md5     " + toHexString(md5.digest()));
					LOG.info("");
				}

				LOG.info("Enter certificate to add to trusted keystore or 'q' to quit: [1]");
				int k;
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
					String line = reader.readLine().trim();
					k = (line.length() == 0) ? 0 : Integer.parseInt(line) - 1;
				} catch (Exception e) {
					LOG.info("KeyStore not changed");
					k = -1;
				}

				return k;
			}
		};

		/**
		 * @return the index of the chosen certificate. -1 means abort this process.
		 */
		public abstract int chooseCertificate(X509Certificate[] chain) throws NoSuchAlgorithmException, CertificateEncodingException;
	}

	Mode mode;

	/**
	 * add a certificate for the specified host in the keystore file.
	 * defaults:
	 *   port 443
	 *   passphrase CHANGE_IT
	 *
	 * @param hostName
	 *          required
	 * @throws IOException
	 * @throws KeyStoreException
	 * @throws CertificateException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public KeyStore installCert(@NonNull String hostName) throws CertToolException, NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, KeyManagementException {
		return installCert(hostName, 443, CHANGE_IT);
	}

	/**
	 * add a certificate for the specified host in the keystore file
	 *
	 * @param hostName
	 *          required
	 * @param port
	 *          required
	 * @param password
	 *          required
	 * @return true = successful. false = process aborted.
	 * @throws IOException
	 * @throws KeyStoreException
	 * @throws CertificateException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public KeyStore installCert(@NonNull String hostName, int port, @NonNull char[] passphrase) throws CertToolException, NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, KeyManagementException {
		KeyStore keyStore = loadKeyStore(passphrase);

		// set up for ssl connection
		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(keyStore);
		X509TrustManager defaultTrustManager = (X509TrustManager) trustManagerFactory.getTrustManagers()[0];
		SavingTrustManager savingTrustManager = new SavingTrustManager(defaultTrustManager);
		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, new TrustManager[] { savingTrustManager }, null);
		SSLSocketFactory factory = context.getSocketFactory();

		// start an ssl connection to see if we succeed for fail
		LOG.info("Opening connection to " + hostName + ":" + port + "...");
		SSLSocket socket = (SSLSocket) factory.createSocket(hostName, port);
		socket.setSoTimeout(10000);
		try {
			LOG.info("Starting SSL handshake...");
			socket.startHandshake();
			socket.close();
			LOG.info("Handshake successful. " + hostName + "'s certificate is already trusted. Nothing to do. Keystore unchanged.");
			return keyStore;
		} catch (SSLException e) {
			LOG.info(e.getMessage());
		}

		// no certificate found in our keystore for this host. we need to add one.
		X509Certificate[] chain = savingTrustManager.certChain;
		if (chain == null) {
			throw new CertToolException("Could not obtain " + hostName + "'s certificate chain. Process aborted. Keystore unchanged.");
		}
		int certIndex = mode.chooseCertificate(chain);
		if (certIndex == -1) {
			throw new CertToolException("No certificate chosen. Process aborted. Keystore unchanged.");
		}

		// add the chosen certificate to our JSSECACERTS keystore.
		X509Certificate cert = chain[certIndex];
		String alias = hostName + "-" + (certIndex + 1);
		keyStore.setCertificateEntry(alias, cert);

		File outputKeyStoreFile = getOutputKeyStoreFile();
		try (OutputStream out = new FileOutputStream(outputKeyStoreFile)) {
			keyStore.store(out, passphrase);
		}

		// we're done
		LOG.info("\n" + cert.toString() + "\n\nAdded certificate to keystore '" + JSSECACERTS + "' using alias '" + alias + "'");

		return keyStore;
	}

	File getInputKeyStoreFile() {
		File keyStoreFile = new File(JSSECACERTS);
		if (keyStoreFile.isFile() == false) {
			File javaHome = getJavaHome();
			keyStoreFile = new File(javaHome, JSSECACERTS);
			if (keyStoreFile.isFile() == false) {
				keyStoreFile = new File(javaHome, CACERTS);
			}
		}
		return keyStoreFile;
	}

	File getOutputKeyStoreFile() {
		File keyStoreFile = new File(JSSECACERTS);
		if (keyStoreFile.isFile() == false) {
			File javaHome = getJavaHome();
			keyStoreFile = new File(javaHome, JSSECACERTS);
		}
		return keyStoreFile;
	}

	File getJavaHome() {
		File javaHome = new File(System.getProperty("java.home") + File.separatorChar + "lib" + File.separatorChar + "security");
		return javaHome;
	}

	KeyStore loadKeyStore() throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		return loadKeyStore(getInputKeyStoreFile(), CHANGE_IT);
	}

	KeyStore loadKeyStore(@NonNull char[] passphrase) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		return loadKeyStore(getInputKeyStoreFile(), passphrase);
	}

	KeyStore loadKeyStore(@NonNull File keyStoreFile, @NonNull char[] passphrase) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		LOG.info("Loading KeyStore " + keyStoreFile + "...");
		KeyStore keyStore;
		try (InputStream inputStream = new FileInputStream(keyStoreFile)) {
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(inputStream, passphrase);
		}
		return keyStore;
	}

	static final char[] HEXDIGITS = "0123456789abcdef".toCharArray();

	static String toHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 3);
		for (int b : bytes) {
			b &= 0xff;
			sb.append(HEXDIGITS[b >> 4]);
			sb.append(HEXDIGITS[b & 15]);
			sb.append(' ');
		}
		return sb.toString();
	}

	static class SavingTrustManager implements X509TrustManager {

		private final X509TrustManager	trustManager;
		private X509Certificate[]				certChain;

		SavingTrustManager(X509TrustManager trustManager) {
			this.trustManager = trustManager;
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			throw new UnsupportedOperationException();
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			this.certChain = chain;
			trustManager.checkServerTrusted(chain, authType);
		}
	}

	public static void main(String[] args) {
		// capture params
		String host;
		int port;
		String passphrase;
		if ((args.length == 1) || (args.length == 2)) {
			String[] c = args[0].split(":");
			host = c[0];
			port = (c.length == 1) ? 443 : Integer.parseInt(c[1]);
			passphrase = (args.length == 1) ? "changeit" : args[1];   // you didn't specify a password. so we'll use "changeit" as the password.
		} else {
			System.out.println("Usage: java CertTool hostname[:port] [passphrase]");
			System.out.println("Example: % java CertTool ecc.fedora.redhat.com");
			return;
		}

		// do it
		try {
			CertTool.builder().mode(Mode.COMMAND_LINE).build().installCert(host, port, passphrase.toCharArray());
		} catch (CertToolException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.exit(0);
	}
}