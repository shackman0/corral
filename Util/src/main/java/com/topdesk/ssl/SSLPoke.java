package com.topdesk.ssl;

import java.io.InputStream;
import java.io.OutputStream;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Establish a SSL connection to a host and port, writes a byte and prints the response.
 * See
 * http://confluence.atlassian.com/display/JIRA/Connecting+to+SSL+services
 * https://confluence.atlassian.com/download/attachments/117455/SSLPoke.java
 */
public class SSLPoke {

	final static Logger LOG = LoggerFactory.getLogger(SSLPoke.class);

	private SSLPoke() {
		// do not use
	}

	/**
	 * uses default ssl port 443
	 *
	 * @param hostName
	 * @return true = poke ok; false = poke failed
	 */
	public static boolean poke(String hostName) {
		return poke(hostName, 443);
	}

	/**
	 * @param hostName
	 * @param port
	 * @return true = poke ok; false = poke failed
	 */
	public static boolean poke(String hostName, int port) {
		try {
			SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
			SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(hostName, port);

			InputStream in = sslsocket.getInputStream();
			OutputStream out = sslsocket.getOutputStream();

			// Write a test byte to get a reaction :)
			out.write(1);

			while (in.available() > 0) {
				LOG.info(String.valueOf(in.read()));
			}

			return true;   // poke ok

		} catch (Exception e) {
			e.printStackTrace();
			return false; // poke failed
		}
	}

  public static int main(String[] args) {
	if (args.length != 2) {
		LOG.info("Usage: "+SSLPoke.class.getName()+" <host> <port>");
		System.exit(1);
	}
	boolean rc = poke(args[0], Integer.valueOf(args[1]));
	return ((rc == true) ? 0 : 1);
  }
}
