package com.topdesk.messagerouter.client;


/**
 * IRunnable
 * <p>
 * Let's me quiesce() the process instead of killing it
 */

public interface IRunnable extends Runnable {

	public void quiesce() throws Exception;

}
