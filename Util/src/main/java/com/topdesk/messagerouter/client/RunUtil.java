package com.topdesk.messagerouter.client;

import lombok.Getter;

/**
 * RunUtil
 * <p>
 * run until shutdown by user
 */

public class RunUtil {



	/**
	 * the thread in which the runnable is running
	 */
	@Getter
	private Thread runThread;

	/**
	 * run the runnable until the user enters 'Q' on the console
	 *
	 * @param class2Run
	 * @throws Exception
	 */
  public void run(IRunnable runnable) throws Exception {
  	runThread = new Thread(runnable);
  	runThread.setDaemon(true);
    runThread.start();

    System.out.println("press Q THEN ENTER to terminate");

    boolean running = true;
    try {
    	while(running){
    		if(('Q' == System.in.read())) {
    			System.out.println("shutting down now");
    			running = false;
    		}
    	}
    } finally {
    	runThread.interrupt();
    	runnable.quiesce();
    }
  }

}
