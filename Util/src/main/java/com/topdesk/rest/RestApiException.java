package com.topdesk.rest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.http.Header;
import org.apache.http.RequestLine;
import org.apache.http.StatusLine;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RestApiException extends Exception {

	private static final long	serialVersionUID	= 451769271573346188L;

	private String      message;
	private RequestLine requestLine;
	private InputStream requestEntityContent;
	private StatusLine	statusLine;
	private String			responseBody;
	private Header[]		headers;

	public RestApiException(String message) {
		super(message);
		this.message = message;
	}

	public RestApiException(Throwable throwable) {
		super(throwable);
		this.message = throwable.getMessage();
	}

	public RestApiException(String message, Throwable throwable) {
		super(message, throwable);
		this.message = message;
	}

	@Override
	public String getMessage() {
		StringBuilder fullMessage = new StringBuilder();
		if (message != null) {
			fullMessage.append(message);
		}
		if (statusLine != null) {
			fullMessage.append("\n");
			fullMessage.append(statusLine.toString());
		}
		if (responseBody != null) {
			fullMessage.append("\n");
			fullMessage.append(responseBody);
		}
		if (requestLine != null) {
			fullMessage.append("\n");
			fullMessage.append(requestLine.getProtocolVersion());
			fullMessage.append(" ");
			fullMessage.append(requestLine.getMethod());
			fullMessage.append(" ");
			fullMessage.append(requestLine.getUri());
		}
		if (headers != null) {
			Arrays.stream(headers).forEach(header -> {
				fullMessage.append("\n");
				fullMessage.append(header.getName());
				fullMessage.append("=");
				fullMessage.append(header.getValue());
				});
			if (requestEntityContent != null) {
				String requestBody = new BufferedReader(new InputStreamReader(requestEntityContent)).lines().parallel().collect(Collectors.joining("\n"));
				fullMessage.append("\n");
				fullMessage.append(requestBody);
			}
		}

		return fullMessage.toString();
	}

}
