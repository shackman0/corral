package com.topdesk.iot.util;

import java.io.IOException;
import java.util.Map;

import org.junit.Test;

import com.topdesk.util.HttpStatusCodes;

public class HttpStatusCodesTest {

	@Test
	public void readAndParseFile() throws IOException {
		Map<Integer, String> statusCodes = HttpStatusCodes.getStatusCodes();
		statusCodes.forEach((k,v)->System.out.println(k + " " + v));
	}
}
