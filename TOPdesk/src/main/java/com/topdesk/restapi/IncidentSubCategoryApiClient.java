package com.topdesk.restapi;

import java.util.function.Predicate;

import com.topdesk.iot.dtos.SubCategoryDto;

/**
 * CategoryAPIClient
 * <p>
 * client to TOPdesk Category API
 */
public class IncidentSubCategoryApiClient implements IRestApiClient<SubCategoryDto, IncidentSubCategoryApiClient> {

	public static final String INCIDENT_SUBCATEGORIES_API_PATH = IncidentApiClient.INCIDENT_API_PATH + "/subcategories";

	public IncidentSubCategoryApiClient() {
		// do nothing
	}

	@Override
	public String getFetchListApiPath() {
		return INCIDENT_SUBCATEGORIES_API_PATH;
	}

	@Override
	public Class<SubCategoryDto> getDtoClass() {
		return SubCategoryDto.class;
	}

	@Override
	public Predicate<SubCategoryDto> getDefaultPredicate(String... strings) {
		return (dto) -> (dto.getName().equals(strings[0]));
	}

}
