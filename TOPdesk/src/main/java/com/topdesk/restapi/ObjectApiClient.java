package com.topdesk.restapi;

import com.topdesk.iot.configuration.Configuration;

/**
 * ObjectApiClient
 * DO NOT USE: There is no Object API yet ...
 * <p>
 * client to TOPdesk Category API
 */
public class ObjectApiClient /* implements IApiClient<ObjectDto, ObjectApiClient> */ {

	public static final String OBJECT_API_PATH = Configuration.SUBSCRIBER_TOPDESK_API_ROOT_URL.value() + "/objects";

	public ObjectApiClient() {
		// do nothing
	}

/*
	@Override
	public String getFetchListPath() {
		return OBJECT_API_PATH;
	}

	@Override
	public Class<ObjectDto> getDtoClass() {
		return ObjectDto.class;
	}

	@Override
	public Predicate<ObjectDto> getPredicate(String... strings) {
		return (dto) -> (dto.getName().equals(strings[0]));
	}
*/

}
