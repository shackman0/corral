package com.topdesk.restapi;

import java.util.function.Predicate;

import com.topdesk.iot.dtos.IncidentProcessingStatusDto;

/**
 * IncidentProcessingStatusApiClient
 * <p>
 * client to TOPdesk incident processing status API
 */
public class IncidentProcessingStatusApiClient implements IRestApiClient<IncidentProcessingStatusDto, IncidentProcessingStatusApiClient> {

	public static final String INCIDENT_PROCESSING_STATUS_API_PATH = IncidentApiClient.INCIDENT_API_PATH + "/processing_status";

	public IncidentProcessingStatusApiClient() {
		// do nothing
	}

	@Override
	public String getFetchListApiPath() {
		return INCIDENT_PROCESSING_STATUS_API_PATH;
	}

	@Override
	public Class<IncidentProcessingStatusDto> getDtoClass() {
		return IncidentProcessingStatusDto.class;
	}

	@Override
	public Predicate<IncidentProcessingStatusDto> getDefaultPredicate(String... strings) {
		return (dto) -> (dto.getName().equals(strings[0]));
	}

}
