package com.topdesk.restapi;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.dtos.OperatorDto;
import com.topdesk.rest.RestApiException;
import com.topdesk.util.OutParameter;

/**
 * OperatorAPIClient
 * <p>
 * client to TOPdesk Operator API
 */
public class OperatorApiClient implements IRestApiClient<OperatorDto, OperatorApiClient> {

	public static final String OPERATORS_API_PATH = Configuration.SUBSCRIBER_TOPDESK_API_ROOT_URL.value() + "/operators";

	public OperatorApiClient() {
		// do nothing
	}

	@Override
	public String getFetchListApiPath() {
		return OPERATORS_API_PATH;
	}

	@Override
	public Class<OperatorDto> getDtoClass() {
		return OperatorDto.class;
	}

	@Override
	public Predicate<OperatorDto> getDefaultPredicate(String... strings) {
		return (dto) -> (dto.getFirstName().equals(strings[0]) && ((strings.length == 2) && (strings[1] != null) && (strings[1].isEmpty() == false) && (dto.getSurName().equals(strings[1]))));
	}

	@Override
	public OperatorDto findUnique(String authenticationToken, String... strings) throws RestApiException {
		String lastName = strings[0];
		String firstName = strings.length > 1 ? strings[1] : null;
		Collection<OperatorDto> dtos = fetchHavingName(authenticationToken, lastName, firstName);
		if (dtos.size() == 0) {
			return null;
		}
		if (dtos.size() == 1) {
			return dtos.iterator().next();
		}
		throw RestApiException.builder().message(String.format("found %d matches for %s", dtos.size(), Arrays.toString(strings))).build();
	}

	@Override
	public Collection<OperatorDto> find(String authenticationToken, String... strings) throws RestApiException {
		Collection<OperatorDto> collection = fetchLargeList(authenticationToken);
		Collection<OperatorDto> operatorDtos = collection.stream().filter(getDefaultPredicate(strings)).collect(Collectors.toList());

		return operatorDtos;
	}

	public Collection<OperatorDto> fetchHavingName(String authenticationToken, String lastName, String firstName) throws RestApiException {
		StringBuilder parameters = new StringBuilder();
		parameters.append("?");

		try {
			if ((lastName != null) && (lastName.trim().isEmpty() == false)) {
				parameters.append("lastname=");
				parameters.append(URLEncoder.encode(lastName.trim(), "UTF-8"));
			}
			if ((firstName != null) && (firstName.trim().isEmpty() == false)) {
				if (parameters.length() > 1) {   // we have a lastName param
					parameters.append("&");
				}
				parameters.append("firstname=");
				parameters.append(URLEncoder.encode(firstName.trim(), "UTF-8"));
			}
		} catch (UnsupportedEncodingException e) {
			throw new RestApiException(e);
		}

		String path = OPERATORS_API_PATH + parameters.toString();
		OutParameter<String> jsonResponseCarrier = new OutParameter<>();
		IRestApiClient.fetchAsJson(authenticationToken, path, jsonResponseCarrier, HttpURLConnection.HTTP_OK);
		Collection<OperatorDto> operatorDtos = IRestApiClient.convertJson2Dtos(jsonResponseCarrier.getPayload(), OperatorDto.class);

		return operatorDtos;
	}

}
