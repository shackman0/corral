package com.topdesk.restapi;

import java.io.StringReader;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;

import com.topdesk.browserdriver.Client;
import com.topdesk.iot.dtos.IDto;
import com.topdesk.rest.RestApiException;
import com.topdesk.util.OutParameter;

import flexjson.JSONDeserializer;

public interface IRestApiClient<T extends IDto<T>, U extends IRestApiClient<T,U>> {

	String getFetchListApiPath();
	Class<T> getDtoClass();
	Predicate<T> getDefaultPredicate(String... strings);

	/**
	 * fetch a list from the API. override this if you want to cache the list locally
	 * @param authenticationToken
	 * @return
	 * @throws RestApiException
	 */
	default Collection<T> fetchList(String authenticationToken) throws RestApiException {
		return fetchList(authenticationToken, getFetchListApiPath(), getDtoClass());
	}

	default Collection<T> fetchLargeList(String authenticationToken) throws RestApiException {
		return fetchLargeList(authenticationToken, getFetchListApiPath(), getDtoClass());
	}

	/**
	 * find the dtos matching the supplied strings in the collection using the default predicate
	 * @param collection
	 * @param strings
	 * @return
	 */
	default Collection<T> find(Collection<T> collection, String... strings) {
		Collection<T> dtos = collection.stream().filter(getDefaultPredicate(strings)).collect(Collectors.toList());
		return dtos;
	}

	/**
	 * find the unique dto matching the supplied strings in the collection using the default predicate
	 * @param collection
	 * @param strings
	 * @return
	 * @throws RestApiException if there is more than onw matching dto; null if no matches
	 */
	default T findUnique(Collection<T> collection, String... strings) throws RestApiException {
		Collection<T> dtos = find(collection, strings);
		if (dtos.size() == 0) {
			return null;
		}
		if (dtos.size() == 1) {
			return dtos.iterator().next();
		}
		throw RestApiException.builder().message(String.format("found %d matches for %s", dtos.size(), Arrays.toString(strings))).build();
	}

	/**
	 * fetches the list and finds the dtos matching the supplied strings using the default predicate
	 * @param authenticationToken
	 * @param strings
	 * @return
	 * @throws RestApiException
	 */
	default Collection<T> find(String authenticationToken, String... strings) throws RestApiException {
		Collection<T> dtos = fetchList(authenticationToken);
		return find(dtos,strings);
	}

	/**
	 * fetches the list and finds the unique dto matching the supplied strings using the default predicate
	 * @param authenticationToken
	 * @param strings
	 * @return
	 * @throws RestApiException if there is more than onw matching dto; null if no matches
	 */
	default T findUnique(String authenticationToken, String... strings) throws RestApiException {
		Collection<T> dtos = find(authenticationToken, strings);
		if (dtos.size() == 0) {
			return null;
		}
		if (dtos.size() == 1) {
			return dtos.iterator().next();
		}
		throw RestApiException.builder().message(String.format("found %d matches for %s", dtos.size(), Arrays.toString(strings))).build();
	}

	/**
	 * fetches the list and finds the dtos using the supplied predicate
	 * @param authenticationToken
	 * @param predicate
	 * @return
	 * @throws RestApiException
	 */
	default Collection<T> find(String authenticationToken, Predicate<T> predicate) throws RestApiException {
		Collection<T> collection = fetchList(authenticationToken);
		Collection<T> dtos = collection.stream().filter(predicate).collect(Collectors.toList());
		return dtos;
	}

	/**
	 * fetches the list and finds the unique dto using the supplied predicate
	 * @param authenticationToken
	 * @param predicate
	 * @return
	 * @throws RestApiException if there is more than onw matching dto; null if no matches
	 */
	default T findUnique(String authenticationToken, Predicate<T> predicate) throws RestApiException {
		Collection<T> dtos = find(authenticationToken);
		if (dtos.size() == 0) {
			return null;
		}
		if (dtos.size() == 1) {
			return dtos.iterator().next();
		}
		throw RestApiException.builder().message(String.format("found %d matches", dtos.size())).build();
	}

	static <T extends IDto<T>> Collection<T> fetchList(String authenticationToken, String apiPath, Class<T> classT) throws RestApiException {
		OutParameter<String> jsonResponseCarrier = new OutParameter<>();
		fetchAsJson(authenticationToken, apiPath, jsonResponseCarrier);
		Collection<T> list = convertJson2Dtos(jsonResponseCarrier.getPayload(), classT);

		return list;
	}

	static <T extends IDto<T>> Collection<T> fetchLargeList(String authenticationToken, String apiPath, Class<T> classT, Integer... nonErrorResponseCodes) throws RestApiException {
		Collection<T> list = new HashSet<>();
		OutParameter<String> jsonResponseCarrier = new OutParameter<>();
		StatusLine statusLine;
		int start = 0;
		do {
			String path = apiPath + "?start=" + start;
			statusLine = fetchAsJson(authenticationToken, path, jsonResponseCarrier, Integer.valueOf(HttpURLConnection.HTTP_OK), Integer.valueOf(HttpURLConnection.HTTP_PARTIAL));
			Collection<T> dtos = convertJson2Dtos(jsonResponseCarrier.getPayload(), classT);
			list.addAll(dtos);
			start += dtos.size();
		} while (statusLine.getStatusCode() == HttpURLConnection.HTTP_PARTIAL);

		return list;
	}

	static StatusLine fetchAsJson(String authenticationToken, String apiPath, OutParameter<String> jsonResponseCarrier, Integer... nonErrorResponseCodes) throws RestApiException {
		HttpGet httpGet = new HttpGet(apiPath);
		StatusLine statusLine = Client.httpClientExecute(authenticationToken, httpGet, jsonResponseCarrier, nonErrorResponseCodes);

		return statusLine;
	}

	static <T extends IDto<T>> Collection<T> convertJson2Dtos(String json, Class<T> dtoClass) {
		Collection<T> dtos = new JSONDeserializer<Collection<T>>()
			.use("values", dtoClass)
			.deserialize(json, HashSet.class);

		return dtos;
	}

	static JsonObject convertJson2JsonObject(String json) {
		JsonObject jsonObject;
		try (JsonReader jsonReader = Json.createReader(new StringReader(json))) {
			jsonObject = jsonReader.readObject();
		}

		return jsonObject;
	}

}
