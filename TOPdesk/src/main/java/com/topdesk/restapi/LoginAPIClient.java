package com.topdesk.restapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;

import com.topdesk.browserdriver.Client;
import com.topdesk.iot.configuration.Configuration;
import com.topdesk.rest.RestApiException;
import com.topdesk.util.OutParameter;

/**
 * LoginAPIClient
 * <p>
 * login client to TOPdesk API
 *
 * @TODO
 * 1. no logout()???
 * 2. what should a client do when the authenticationToken expires?
 */
public class LoginAPIClient {

	private static final String USER_AGENT = "Mozilla/5.0";
 	private static final String LOGIN_API_ROOT = Configuration.SUBSCRIBER_TOPDESK_API_ROOT_URL.value() + "/login";
 	private static final String LOGIN_OPERATOR_API_PATH = LOGIN_API_ROOT + "/operator";
 	private static final String LOGIN_PERSON_API_PATH = LOGIN_API_ROOT + "/person";

 	enum PrincipalType {
 		PERSON
 		{
 			@Override
			public String getAPIPath() {
 				return LOGIN_PERSON_API_PATH;
 			};
 		},
 		OPERATOR
 		{
 			@Override
			public String getAPIPath() {
 				return LOGIN_OPERATOR_API_PATH;
 			};
 		};
 		public abstract String getAPIPath();
 	}

 	/**
 	 * it's all static
 	 */
 	private LoginAPIClient() {
 		// do nothing
 	}

	/**
	 * Login to the TOPdesk API as the default Corral Operator
	 *
	 * @return authenticationToken
	 * @throws RestApiException
	 */
	public static String loginSubscriber() throws RestApiException {
		return login(PrincipalType.OPERATOR, Configuration.SUBSCRIBER_TOPDESK_API_AUTHENTICATION_USERID.value(), Configuration.SUBSCRIBER_TOPDESK_API_AUTHENTICATION_PASSWORD.value());
	}

	/**
	 * Login to the TOPdesk API as the specified user
	 *
	 * @return authenticationToken
	 */
	public static String login(PrincipalType principalType, String userId, String password) throws RestApiException {
			OutParameter<String> response = new OutParameter<>();
			login(principalType, userId, password, response);
			String authenticationToken = response.getPayload();

			return authenticationToken;
	}

	/**
	 * Login to the TOPdesk API
	 *
	 * @return StatusLine
	 * @throws RestApiException
	 */
	public static StatusLine login(PrincipalType principalType, String userId, String password, OutParameter<String> response, Integer... nonErrorResponseCodes) throws RestApiException {
		StatusLine statusLine = null;
		try {
			TrustStrategy trustStrategy = new TrustSelfSignedStrategy();
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(trustStrategy).build();
			HostnameVerifier hostnameVerifier = NoopHostnameVerifier.INSTANCE;

			try (CloseableHttpClient httpclient = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(hostnameVerifier).build()) {
				HttpGet httpGet = new HttpGet(principalType.getAPIPath());
				httpGet.setHeader("Accept", "*/*");
				httpGet.setHeader("Content-Type", "application/json");
				String encoding = Base64.getEncoder().encodeToString((userId + ":" + password).getBytes());
				httpGet.setHeader("Authorization", "Basic " + encoding);
				httpGet.setHeader("User-Agent", USER_AGENT);
				response.clearPayload();
				try (CloseableHttpResponse httpResponse = httpclient.execute(httpGet)) {
					statusLine = httpResponse.getStatusLine();
					HttpEntity responseEntity = httpResponse.getEntity();
					if (responseEntity != null) {
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
						StringBuffer responseString = new StringBuffer();
						String inputLine;
						while ((inputLine = bufferedReader.readLine()) != null) {
							responseString.append(inputLine);
						}
						response.setPayload(responseString.toString());
					}
				}

				if (Client.isHttpError(statusLine.getStatusCode(), nonErrorResponseCodes)) {
					throw RestApiException.builder().headers(httpGet.getAllHeaders()).requestLine(httpGet.getRequestLine()).responseBody(response.getPayload()).statusLine(statusLine).build();
				}
			}
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			throw new RestApiException(e);
		}

		return statusLine;
	}

}
