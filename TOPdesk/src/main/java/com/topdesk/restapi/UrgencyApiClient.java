package com.topdesk.restapi;

import java.util.function.Predicate;

import com.topdesk.iot.dtos.UrgencyDto;

/**
 * ObjectAPIClient
 * <p>
 * client to TOPdesk Category API
 */
public class UrgencyApiClient implements IRestApiClient<UrgencyDto, UrgencyApiClient> {

	public static final String URGENCY_API_PATH = IncidentApiClient.INCIDENT_API_PATH + "/urgencies";

	public UrgencyApiClient() {
		// do nothing
	}

	@Override
	public String getFetchListApiPath() {
		return URGENCY_API_PATH;
	}

	@Override
	public Class<UrgencyDto> getDtoClass() {
		return UrgencyDto.class;
	}

	@Override
	public Predicate<UrgencyDto> getDefaultPredicate(String... strings) {
		return (dto) -> (dto.getName().equals(strings[0]));
	}

}
