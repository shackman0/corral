package com.topdesk.restapi;

import java.util.function.Predicate;

import com.topdesk.iot.dtos.CategoryDto;

/**
 * CategoryAPIClient
 * <p>
 * client to TOPdesk Category API
 */
public class IncidentCategoryApiClient implements IRestApiClient<CategoryDto, IncidentCategoryApiClient> {

	public static final String INCIDENT_CATEGORIES_API_PATH = IncidentApiClient.INCIDENT_API_PATH + "/categories";

	public IncidentCategoryApiClient() {
		// do nothing
	}

	@Override
	public String getFetchListApiPath() {
		return INCIDENT_CATEGORIES_API_PATH;
	}

	@Override
	public Class<CategoryDto> getDtoClass() {
		return CategoryDto.class;
	}

	@Override
	public Predicate<CategoryDto> getDefaultPredicate(String... strings) {
		return (dto) -> (dto.getName().equals(strings[0]));
	}

}
