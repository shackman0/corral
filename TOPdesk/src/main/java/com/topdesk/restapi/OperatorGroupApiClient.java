package com.topdesk.restapi;

import java.util.function.Predicate;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.dtos.OperatorGroupDto;

/**
 * OperatorGroupAPIClient
 * <p>
 * client to TOPdesk OperatorGroup API
 */
public class OperatorGroupApiClient implements IRestApiClient<OperatorGroupDto, OperatorGroupApiClient> {

	public static final String OPERATOR_GROUPS_API_PATH = Configuration.SUBSCRIBER_TOPDESK_API_ROOT_URL.value() + "/operatorgroups";

	public OperatorGroupApiClient() {
		// do nothing
	}

	@Override
	public String getFetchListApiPath() {
		return OPERATOR_GROUPS_API_PATH;
	}

	@Override
	public Class<OperatorGroupDto> getDtoClass() {
		return OperatorGroupDto.class;
	}

	@Override
	public Predicate<OperatorGroupDto> getDefaultPredicate(String... strings) {
		return (dto) -> (dto.getGroupName().equals(strings[0]));
	}

}
