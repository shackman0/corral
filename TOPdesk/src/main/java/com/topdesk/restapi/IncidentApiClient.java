package com.topdesk.restapi;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.json.JsonObject;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.browserdriver.Client;
import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.dtos.IncidentDto;
import com.topdesk.rest.RestApiException;
import com.topdesk.util.OutParameter;

/**
 * IncidentAPIClient
 * <p>
 * client to TOPdesk Incident API
 */
public class IncidentApiClient {

	static final Logger LOG = LoggerFactory.getLogger(IncidentApiClient.class);

	public static final String INCIDENT_API_PATH = Configuration.SUBSCRIBER_TOPDESK_API_ROOT_URL.value() + "/incidents";
	private static final String GET_INCIDENT_BY_ID_FORMAT = INCIDENT_API_PATH + "/id/%s";

	/**
	 * everything's static
	 */
	private IncidentApiClient() {
		// do nothing
	}

	/**
	 * API
	 * @param authenticationToken
	 * @param incidentUuid in canonical form
	 * @param jsonResponseCarrier incident in json form
	 * @return
	 * @throws RestApiException
	 */
	public static StatusLine getIncidentById(String authenticationToken, String incidentUuid, OutParameter<String> jsonResponseCarrier) throws RestApiException {
		String getIncidentByIdPath = String.format(GET_INCIDENT_BY_ID_FORMAT, incidentUuid.trim());
		HttpGet httpGet = new HttpGet(getIncidentByIdPath);
		StatusLine statusLine = Client.httpClientExecute(authenticationToken, httpGet, jsonResponseCarrier, HttpURLConnection.HTTP_OK, HttpURLConnection.HTTP_NOT_FOUND);

		return statusLine;
	}

	/**
	 * API
	 * @param authenticationToken
	 * @param incidentDto
	 * @param requestJson the request Json is placed here
	 * @param response the response from the server is placed here
	 * @return
	 * @throws RestApiException
	 * @throws UnsupportedEncodingException
	 */
	public static StatusLine submitIncident(String authenticationToken, IncidentDto incidentDto, OutParameter<String> requestJson, OutParameter<String> response) throws RestApiException, UnsupportedEncodingException {
		String incidentJson = constructIncidentJson(incidentDto);
		requestJson.setPayload(incidentJson);
		StatusLine statusLine = createIncident(authenticationToken, incidentJson, response);

		return statusLine;
	}

	static StatusLine createIncident(String authenticationToken, String incidentJson, OutParameter<String> response) throws RestApiException, UnsupportedEncodingException {
		HttpPost httpPost = new HttpPost(INCIDENT_API_PATH);
//		LOG.info(LogMessage.msg(incidentJson));
		StringEntity requestEntity = new StringEntity(incidentJson);
		httpPost.setEntity(requestEntity);
		StatusLine statusLine = Client.httpClientExecute(authenticationToken, httpPost, response, HttpURLConnection.HTTP_CREATED);

		return statusLine;
	}

	static String constructIncidentJson(IncidentDto incidentDto) {
    String incidentJson = Client
    	.JSON_SERIALIZER
    	// we only want Id fields for outbound requests
    	.include("object.id", "category.id", "subcategory.id", "operator.id", "operatorGroup.id", "urgency.id", "caller.id")
    	.exclude("id", "object.*", "category.*", "subcategory.*", "operator.*", "operatorGroup.*", "urgency.*", "caller.*")
    	.deepSerialize(incidentDto);

    return incidentJson;
	}

	/**
	 * @param authenticationToken
	 * @param incidentNumber
	 * @param incidentUuid in canonical form
	 * @param jsonObjectCarrier jsonObject of the incident. null if cannot get the incident.
	 * @return StatusLine
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 * @throws IOException
	 * @throws RestApiException
	 */
	public static StatusLine getIncidentAsJsonObject(String authenticationToken, String incidentUuid, OutParameter<JsonObject> jsonObjectCarrier) throws RestApiException {
		OutParameter<String> jsonResponseCarrier = new OutParameter<>();
		StatusLine statusLine = getIncidentById(authenticationToken, incidentUuid, jsonResponseCarrier);
		int statusCode = statusLine.getStatusCode();
		String incidentJson = jsonResponseCarrier.getPayload();
		if (statusCode == HttpURLConnection.HTTP_OK) {
			JsonObject jsonObject = IRestApiClient.convertJson2JsonObject(incidentJson);
			jsonObjectCarrier.setPayload(jsonObject);
		}

		return statusLine;
	}

}
