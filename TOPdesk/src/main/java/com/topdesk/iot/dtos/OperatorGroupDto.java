package com.topdesk.iot.dtos;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(access=AccessLevel.PRIVATE)
@NoArgsConstructor
@Data
@Builder
public class OperatorGroupDto implements IDto<OperatorGroupDto>, IOperatorOrOperatorGroup {

	private String id;
	private String status;
	private String groupName;

//	private BranchDto branch;
//	private LocationDto location;
//
//	private SearchlistDto budgetHolder;
//	private BigDecimal hourlyRate = BigDecimal.ZERO;
//	private OperatorDto contact;

}
