package com.topdesk.iot.dtos;

import java.util.Date;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(access=AccessLevel.PRIVATE)
@NoArgsConstructor
@Data
@Builder
public class IncidentDto implements IDto<IncidentDto> {

	private String id;

	private String status;
	private String request;
	private String action;
	private String briefDescription;
	private Date targetDate;
	private Date callDate;
	private CategoryDto category;
	private SubCategoryDto subcategory;
	private IOperatorOrOperatorGroup operator;
	private OperatorGroupDto operatorGroup;
	private UrgencyDto urgency;
	private PersonDto caller;
	private IncidentProcessingStatusDto processingStatus;
	private boolean completed;
	private Date completedDate;
	private boolean closed;
	private Date closedDate;

//	private ObjectDto object;
//	private String number;

//	private BranchDto callerBranch;
//	private SearchlistDto branchExtraFieldA;
//	private SearchlistDto branchExtraFieldB;

//	private String externalNumber;
//	private SearchlistDto callType;
//	private SearchlistDto entryType;

//	private ObjectDto object;
//	private BranchDto branch;
//	private LocationDto location;

//	private SearchlistDto impact;
//	private SearchlistDto priority;
//	private SearchlistDto duration;
//	private boolean onHold;
//	private Date onHoldDate;
//	private long onHoldDuration;

//	private SupplierDto supplier;
//	private SearchlistDto processingStatus;
//	private SearchlistDto closureCode;
//	private long timeSpent;
//	private BigDecimal costs;

//	private SearchlistDto creator;
//	private Date creationDate;
//	private SearchlistDto modifier;
//	private Date modificationDate;

//	private boolean majorCall;
//	private MajorIncidentDto majorCallObject;
//	private boolean publishToSsd;

//	private boolean monitored;
//	private long expectedTimeSpent;

//	private OptionalFieldsDto optionalFields1;
//	private OptionalFieldsDto optionalFields2;
}
