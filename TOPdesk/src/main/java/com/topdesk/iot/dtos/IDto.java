package com.topdesk.iot.dtos;

public interface IDto<T extends IDto<T>> {

	String getId();

}
