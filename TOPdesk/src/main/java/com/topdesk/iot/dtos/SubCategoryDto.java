package com.topdesk.iot.dtos;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(access=AccessLevel.PRIVATE)
@NoArgsConstructor
@Data
@Builder
public class SubCategoryDto implements IDto<SubCategoryDto> {

	private String id;
	private String name;
	private CategoryDto category;

}
