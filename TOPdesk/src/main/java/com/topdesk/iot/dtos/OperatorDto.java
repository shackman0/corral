package com.topdesk.iot.dtos;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(access=AccessLevel.PRIVATE)
@NoArgsConstructor
@Data
@Builder
public class OperatorDto implements IDto<OperatorDto>, IOperatorOrOperatorGroup {

	// General
	private String id;
	private String surName;
	private String firstName;
	private String status;

/*

	private String dynamicName;
	private String initials;
	private String prefixes;
	private String birthName;
	private String title;
	private Gender gender;
	private SearchlistDto language;

	// Location
	private BranchDto branch;
	private LocationDto location;

	// Contact Details
	private String telephone;
	private String mobileNumber;
	private String faxNumber;
	private String email;
	private String exchangeAccount;

	// TOPdesk Login
	private String loginName;
	private boolean loginPermission;

	// Details
	private String jobTitle;
	private SearchlistDto department;
	private SearchlistDto budgetHolder;
	private String employeeNumber;
	private BigDecimal hourlyRate = BigDecimal.ZERO;
	private String networkLoginName;
	private String mainFrameLoginName;

	// Information tab
	private boolean hasAttention;
	private SearchlistDto attention;
	private String comments;
*/

}
