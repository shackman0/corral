package com.topdesk.iot.dtos;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(access=AccessLevel.PRIVATE)
@NoArgsConstructor
@Data
@Builder
public class IncidentProcessingStatusDto implements IDto<IncidentProcessingStatusDto> {

	private String id;
	private String name;
	private String onHold;
	private String processingState;

	public boolean isActive() {
		return ("CLOSED".equals(processingState) || "COMPLETED".equals(processingState));
	}

}
