package com.topdesk.restapi.test;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.dtos.IDto;
import com.topdesk.rest.RestApiException;
import com.topdesk.restapi.IRestApiClient;
import com.topdesk.restapi.IncidentCategoryApiClient;
import com.topdesk.restapi.IncidentSubCategoryApiClient;
import com.topdesk.restapi.OperatorApiClient;
import com.topdesk.restapi.OperatorGroupApiClient;
import com.topdesk.restapi.UrgencyApiClient;


@RunWith(Parameterized.class)
public class FindTest {

	@Parameters
	public static Collection<Object[]> data() throws ReflectiveOperationException {
		ArrayList<Object[]> data = new ArrayList<>();
		data.add(new Object[] {new IncidentCategoryApiClient(), new String[] { Configuration.INCIDENT_CATEGORY.value() } });
		data.add(new Object[] {new IncidentSubCategoryApiClient(), new String[] { Configuration.INCIDENT_SUB_CATEGORY.value() } });
//		data.add(new Object[] {new ObjectApiClient(), new String[] { "PRN1051" } });
		data.add(new Object[] {new OperatorApiClient(), Configuration.INCIDENT_OPERATOR_SURNAME.value() });
		data.add(new Object[] {new OperatorGroupApiClient(), new String[] { Configuration.INCIDENT_OPERATOR_GROUP.value() } });
		data.add(new Object[] {new UrgencyApiClient(), new String[] { Configuration.INCIDENT_URGENCY.value() } });

		return data;
	}

	@SuppressWarnings("rawtypes")
	IRestApiClient apiClient;
	String[] strings;

	@SuppressWarnings("rawtypes")
	public FindTest (IRestApiClient apiClient, String... strings) {
		this.apiClient = apiClient;
		this.strings = strings;
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void find() throws RestApiException {
		Collection<IDto> collection = TestUtils.fetchList(apiClient);
		@SuppressWarnings("unchecked")
		Collection<IDto> dtos = apiClient.find(collection, strings);
		Assert.assertEquals(1, dtos.size());
	}

}
