package com.topdesk.restapi.test;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.topdesk.iot.dtos.IDto;
import com.topdesk.rest.RestApiException;
import com.topdesk.restapi.IRestApiClient;


@RunWith(Parameterized.class)
public class FetchListTest {

	@SuppressWarnings("rawtypes")
	@Parameters
	public static Collection<? extends IRestApiClient> data() throws ReflectiveOperationException {
		return TestUtils.getIRestApiClientInstances();
	}

	@SuppressWarnings("rawtypes")
	IRestApiClient apiClient;

	@SuppressWarnings("rawtypes")
	public FetchListTest (IRestApiClient apiClient) {
		this.apiClient = apiClient;
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void fetchList() throws RestApiException {
		Collection<IDto> dtos = TestUtils.fetchList(apiClient);
		Assert.assertTrue(dtos != null);
		Assert.assertTrue(dtos.size() > 0);
	}

}
