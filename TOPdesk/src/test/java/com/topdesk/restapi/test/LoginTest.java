package com.topdesk.restapi.test;

import org.junit.Assert;
import org.junit.Test;

import com.topdesk.rest.RestApiException;
import com.topdesk.restapi.LoginAPIClient;


public class LoginTest {

	@Test
	public void login() throws RestApiException {
		String authenticationToken = LoginAPIClient.loginSubscriber();
		Assert.assertNotNull(authenticationToken);
		Assert.assertFalse(authenticationToken.trim().isEmpty());
	}

}
