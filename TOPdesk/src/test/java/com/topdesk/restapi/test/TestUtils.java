package com.topdesk.restapi.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.reflections.Reflections;

import com.topdesk.iot.dtos.IDto;
import com.topdesk.rest.RestApiException;
import com.topdesk.restapi.IRestApiClient;
import com.topdesk.restapi.LoginAPIClient;
import com.topdesk.util.LambdaExceptionUtil;


public class TestUtils {

	@SuppressWarnings("rawtypes")
	public static Collection<? extends IRestApiClient> getIRestApiClientInstances() throws ReflectiveOperationException {
		Set<Class<? extends IRestApiClient>> subTypes = new Reflections("com.topdesk.restapi").getSubTypesOf(IRestApiClient.class);
		ArrayList<IRestApiClient> instances = new ArrayList<>();
		subTypes.forEach(LambdaExceptionUtil.rethrowConsumer(subType -> instances.add(subType.newInstance())));
		return instances;
	}

	@SuppressWarnings("rawtypes")
	public static Collection<IDto> fetchList(IRestApiClient apiClient) throws RestApiException {
		String authenticationToken = LoginAPIClient.loginSubscriber();
		@SuppressWarnings("unchecked")
		Collection<IDto> dtos = IRestApiClient.fetchList(authenticationToken, apiClient.getFetchListApiPath(), apiClient.getDtoClass());
//		dtos.forEach(System.out::println);
		return dtos;
	}

}
