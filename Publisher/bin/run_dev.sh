#!/bin/bash

java \
  -Dcom.topdesk.remotebroker.host="pc1795" \
  -Dcom.topdesk.iot.publisher.pollinterval=1 \
  -Dcom.topdesk.iot.publisher.transactionsize=0 \
  -jar ./bin/PrinterMonitor.jar \
| tee ./logs/$(date +"%Y_%m_%d_%I_%M_%p").log
