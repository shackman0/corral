@echo off

java ^
	-Dcom.topdesk.remotebroker.host="pc1795" ^
	-Dcom.topdesk.iot.publisher.pollinterval=1 ^
	-Dcom.topdesk.iot.publisher.transactionsize=0 ^
	-Dcom.topdesk.iot.messaging.persistent="true" ^
	-jar ./bin/PrinterMonitor.jar
