@echo off

rem PrinterMonitor polls printers and publishes alerts as needed

java ^
	-Dcom.topdesk.remotebroker.host="pc1795" ^
	-Dcom.topdesk.iot.publisher.pollinterval=30 ^
	-Dcom.topdesk.iot.publisher.transactionsize=10 ^
  -jar ./bin/PrinterMonitor.jar
