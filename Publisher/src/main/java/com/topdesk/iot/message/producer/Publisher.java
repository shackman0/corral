package com.topdesk.iot.message.producer;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.network.NetworkConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.iot.configuration.Configuration;
import com.topdesk.util.LogMessage;

import lombok.Getter;


/**
 * Convenience class to create a publisher.
 * <p>
 * usage:
 */
public class Publisher {

  static final Logger LOG = LoggerFactory.getLogger(Publisher.class);

  /**
   * how many messages to "batch up" in a transaction
   * 0 = do not batch messages and do not use a transaction
   * >0 = batch messages and use a transaction
   */
  @Getter
  int transactionSize;

  BrokerService embeddedBroker;
	Connection connection;
  Session session;
  ActiveMQConnectionFactory connectionFactory;
  Topic topic;
  MessageProducer producer;
  boolean started = false;
  boolean stopping = false;

	public Publisher () {
		this(Integer.valueOf(Configuration.PUBLISHER_TRANSACTION_SIZE.value()).intValue());
	}

	public Publisher (int transactionSize) {
		this.transactionSize = transactionSize;
	}

	/**
	 * start the message broker infrastructure and get connected using the current transactionSize
	 * upon completion, we are ready to publish
	 * @throws Exception
	 */
	public void start() throws Exception {
		start(transactionSize);
	}

	/**
	 * start the message broker infrastructure and get connected
	 * upon completion, we are ready to publish
	 * to change the transactionSize, you must stop() and start() the Publisher
	 * @throws Exception
	 */
	public void start(int transactionSize) throws Exception {
		if (started == true) {
			throw new RuntimeException("already started");
		}
		if (stopping == true) {
			throw new RuntimeException("in the process of stopping");
		}

		this.transactionSize = transactionSize;
		startEmbeddedBroker();
		// connect to the brokers
		connectionFactory = new ActiveMQConnectionFactory(Configuration.CONNECTION_FACTORY_BROKER_URL.value());
		connection = connectionFactory.createConnection();
    connection.setClientID(Configuration.CLIENT_ID.value());
    boolean transacted = (transactionSize > 0);
		session = connection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);
		topic = session.createTopic(Configuration.TOPIC_NAME.value());
		producer = session.createProducer(topic);
		producer.setDeliveryMode(DeliveryMode.PERSISTENT);
		connection.start();

		started = true;
	}

	/**
	 *
	 * @throws Exception
	 */
	void startEmbeddedBroker() throws Exception {
		embeddedBroker = new BrokerService();
		embeddedBroker.setBrokerName(Configuration.EMBEDDED_BROKER_NAME.value());
		NetworkConnector networkConnector = embeddedBroker.addNetworkConnector(Configuration.EMBEDDED_BROKER_NETWORK_CONNECTOR_TO_REMOTE_BROKER.value());
		networkConnector.setDuplex(true);
		networkConnector.setNetworkTTL(Integer.valueOf(Configuration.HOP_COUNT.value()).intValue());
		embeddedBroker.setUseJmx(Boolean.valueOf(Configuration.EMBEDDED_BROKER_USE_JMX.value()).booleanValue());
		embeddedBroker.getManagementContext().setConnectorPort(Integer.valueOf(Configuration.EMBEDDED_BROKER_JMX_PORT.value()).intValue());
		embeddedBroker.setPersistent(Boolean.valueOf(Configuration.DURABLE_MESSAGES.value()).booleanValue());
		embeddedBroker.start();
		embeddedBroker.waitUntilStarted();
  }

	/**
	 * you must call stop to shut everything down cleanly
	 * @throws Exception
	 */
	public void stop() throws Exception {
		if (!started) {
			throw new RuntimeException("not started");
		}
		if (stopping) {
			throw new RuntimeException("in the process of stopping");
		}

		stopping = true;

		commitSession();

		if (producer != null) {
			producer.close();
			producer = null;
		}

		if (session != null) {
			if (transactionSize > 0) {
				session.commit();
			}
			session.close();
			session = null;
		}

		if (connection != null) {
			connection.close();
			connection = null;
		}

		connectionFactory = null;

    if (embeddedBroker != null) {
    	embeddedBroker.stop();
    	embeddedBroker.waitUntilStopped();
    	embeddedBroker = null;
    }

    started = false;
    stopping = false;
  }


	/**
	 * the number of published messages that have not been commited
	 * used in conjunction with transactionSize to determine when we should commit a block of messages
	 */
	int messageCount = 0;

	/**
	 *
	 * @param deviceInfo
	 */
	public void publish(String messageText) {
		if (!started || stopping) {
			throw new RuntimeException("not started");
		}
		if (stopping) {
			throw new RuntimeException("in the process of stopping");
		}

    try {
    	LOG.info(LogMessage.msg("publishing to " + connectionFactory.getBrokerURL() + "\n" + messageText));
    	TextMessage textMessage = session.createTextMessage(messageText);
    	producer.send(textMessage);

    	if (transactionSize > 0) {
    		messageCount++;
    		if (messageCount % transactionSize == 0) {
    			commitSession();
    		}
    	}
    } catch (Exception e) {
    	e.printStackTrace();
    }
	}

	/**
	 * forces a commit on the session if messages are being batched
	 * resets messageCount to 0
	 * @throws JMSException
	 */
	void commitSession() throws JMSException {
  	if (transactionSize > 0) {
 			session.commit();
 			messageCount = 0;
  	}
	}

}
