package com.topdesk.iot.message.producer;

import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;


/**
 * IoTServer
 * <p>
 * Polls all the specified devices.
 */
public class IotDeviceStatusEvent {

  static final Logger LOG = LoggerFactory.getLogger(IotDeviceStatusEvent.class);

  Publisher publisher;

	public IotDeviceStatusEvent () {
	}

	public void start() throws Exception {
		if (publisher != null) {
			throw new RuntimeException("publisher already started");
		}

		publisher = new Publisher();
		publisher.start();
	}

	public void stop() throws Exception {
		publisher.stop();
		publisher = null;
	}

	public void commit() throws JMSException {
		publisher.commitSession();
	}

	/**
	 *
	 * @param deviceInfo
	 */
	public void publish(IDeviceInfo<IDeviceStatus> deviceInfo) {
    try {
    	String json = deviceInfo.asJson();
    	publisher.publish(json);
    } catch (Exception e) {
    	e.printStackTrace();
    }
	}

}
