package com.topdesk.iot.message.producer.server;

import java.util.Collection;
import java.util.HashSet;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topdesk.browserdriver.CloseableBrowserDriver;
import com.topdesk.iot.configuration.Configuration;
import com.topdesk.iot.devices.AbstractDevice;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;
import com.topdesk.iot.devices.multifunction.kyocera.KyoceraTASKalfa3050ciClient;
import com.topdesk.iot.devices.printers.brotherinkjet.BrotherInkJetClient;
import com.topdesk.iot.devices.printers.brotherlaser.BrotherLaserClient;
import com.topdesk.iot.devices.printers.hpdesignjet.HPDesignJetClient;
import com.topdesk.iot.devices.printers.hpjetdirect.HPJetDirectClient;
import com.topdesk.iot.devices.printers.hplaserjet.HPLaserJetClient;
import com.topdesk.iot.message.producer.IotDeviceStatusEvent;
import com.topdesk.messagerouter.client.IRunnable;
import com.topdesk.messagerouter.client.RunUtil;
import com.topdesk.util.LogMessage;


/**
 * PrinterMonitor
 * <p>
 * Polls all the specified devices and publishes alerts
 * To use:
 * 1. launch activemq in a separate process at Publisher.REMOTE_BROKER_HOST_PORT to serve as the remote message router
 * 2. run this class as a java application
 */

public class PrinterMonitor implements IRunnable {

  static final Logger LOG = LoggerFactory.getLogger(PrinterMonitor.class);

	private Collection<AbstractDevice> deviceTypes;
	private IotDeviceStatusEvent iotDeviceStatusEvent;

	/**
	 * todo: use Guice for a list of all concrete AbstractDevice-s
	 */
	public PrinterMonitor() {
		deviceTypes = new HashSet<>();
		deviceTypes.add(new KyoceraTASKalfa3050ciClient());
		deviceTypes.add(new HPJetDirectClient());
		deviceTypes.add(new HPDesignJetClient());
		deviceTypes.add(new HPLaserJetClient());
		deviceTypes.add(new BrotherInkJetClient());
		deviceTypes.add(new BrotherLaserClient());
	}

	/**
	 * run this service until told to stop
	 * this method blocks and returns only when it's time to stop the service
	 * @throws Exception
	 */
	public void start() throws Exception {
		new RunUtil().run(this);
	}


	/**
	 * milliseconds
	 */
	private static long MILLIS_PER_MINUTE = 60000L;

	private long getPollIntervalMillis() {
		return Long.valueOf(Configuration.PUBLISHER_POLL_INTERVAL.value()) * MILLIS_PER_MINUTE;
	}

	@Override
	public void run() {
		try {
			iotDeviceStatusEvent = new IotDeviceStatusEvent();
			iotDeviceStatusEvent.start();
			long pollIntervalMillis = getPollIntervalMillis();
			while (true) {
				getStatusesAndPublishAlerts();
				iotDeviceStatusEvent.commit();
				LOG.info(LogMessage.msg("pausing for " + Configuration.PUBLISHER_POLL_INTERVAL.value() + " minutes"));
				Thread.sleep(pollIntervalMillis);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void quiesce() throws Exception {
		iotDeviceStatusEvent.stop();
		iotDeviceStatusEvent = null;
  }

	/**
	 * publish the statuses of all the devices of all the device types having alerts
	 * @throws Exception
	 */
	public void getStatusesAndPublishAlerts() throws Exception {
		try (CloseableBrowserDriver browserDriver = new CloseableBrowserDriver()) {
			for (IDeviceInfo<IDeviceStatus> deviceInfo: AbstractDevice.getDeviceInfos()) {
				boolean compatibleDeviceFound = false;
				for (AbstractDevice deviceType: deviceTypes) {
					if (deviceType.canQueryThisDevice(deviceInfo)) {
						compatibleDeviceFound = true;
						deviceType.getDeviceStatuses(browserDriver, deviceInfo);		// capture the current statuses for this device
						if (deviceInfo.isAlert()) {
							iotDeviceStatusEvent.publish(deviceInfo);		// this device needs attention
						}
						break;
					}
				}
				if (compatibleDeviceFound == false) {
					// ignoring this device
					LOG.warn(LogMessage.msg(String.format("No monitor found for %s %s %s", deviceInfo.getObjectId(), deviceInfo.getManufacturer(), deviceInfo.getModel())));
				}
			}
		}
	}

  private static final Predicate<Configuration> EXCLUDE_SUBSCRIBER_OPTIONS = option -> (option.getKey().contains("subscriber") == false);

	private static String usage() {
		StringBuilder usage = new StringBuilder();
		usage.append("usage\n")
			.append("  java [-D options] -jar ")
			.append(PrinterMonitor.class.getSimpleName())
			.append(".jar\n")
			.append("where -D options and their defaults are:\n");
		Configuration.usage(EXCLUDE_SUBSCRIBER_OPTIONS)
			.forEach(optionUsageString -> {
				usage.append("  ").append(optionUsageString).append("\n");
				} );

		return usage.toString();
	}

	private static String configuration() {
		StringBuilder usage = new StringBuilder();
		usage.append("configuration for this execution is:\n");
		Configuration.configuration(EXCLUDE_SUBSCRIBER_OPTIONS)
			.forEach(optionConfigurationString -> {
				usage.append("  ").append(optionConfigurationString).append("\n");
				} );

		return usage.toString();
	}

	/**
	 * @param args
	 * todo
	 * accept a poll interval command line parm
	 */
	public static void main(String... args) {
		try {
			LOG.info(usage());
			LOG.info(configuration());
			new PrinterMonitor().start();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}


}
