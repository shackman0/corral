package com.topdesk.iot.devices.printers.brotherlaser;

import java.util.Collection;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.topdesk.iot.devices.AbstractDevice;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;

public class BrotherLaserClient extends AbstractDevice {

	// web pages
//	static String	HOME						= "printer/main.html";
	static String	CONFIGURATION		= "printer/configu.html";

	public static String STATUS_OK = "OK";

	public BrotherLaserClient() {
		super();
	}

	@Override
	protected String getStatusPagePath() {
		return CONFIGURATION;
	}

	/**
	 * we check the following statuses:
	 * 1. toner
	 * 2. drum unit
	 * 3. fuser unit
	 * 4. laser unit
	 */
	@Override
	protected Collection<IDeviceStatus> extractDeviceStatuses(Document dom) {
		Collection<IDeviceStatus> deviceStatuses = new HashSet<>();

		deviceStatuses.add(extractDeviceStatus("^*Toner", 2, dom));
		deviceStatuses.add(extractDeviceStatus("^Drum Unit", 2, dom));
		deviceStatuses.add(extractDeviceStatus("^Fuser Unit", 1, dom));
		deviceStatuses.add(extractDeviceStatus("^Laser Unit", 1, dom));

		return deviceStatuses;
	}

	IDeviceStatus extractDeviceStatus(String label, int childIndex, Document dom) {
		Elements elementsMatchingOwnText = dom.getElementsMatchingOwnText(label);
		Element matchingElement = elementsMatchingOwnText.first();
		Element parentTRElement = matchingElement.parent().parent();
		Element statusElement = parentTRElement.child(childIndex);
		String status = statusElement.ownText();
		if (status.startsWith(STATUS_OK)) {
			status = STATUS_OK;    // strip off the trailing graph chars
		}
		return new DeviceStatus(label, status);
	}


	@Override
	protected String getProtocol() {
		return HTTP;
	}

	private static final String MANUFACTURER = "Brother";
	private static final String MODEL = "HL-5380DN";

	@Override
	public boolean canQueryThisDevice(IDeviceInfo<IDeviceStatus> deviceInfo) {
		return ((deviceInfo.getManufacturer().equals(MANUFACTURER)) && (deviceInfo.getModel().equals(MODEL)));
	}

}
