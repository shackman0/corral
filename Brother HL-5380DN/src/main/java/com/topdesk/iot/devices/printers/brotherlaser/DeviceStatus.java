package com.topdesk.iot.devices.printers.brotherlaser;

import com.topdesk.iot.devices.AbstractDeviceStatus;

public class DeviceStatus extends AbstractDeviceStatus {

	/**
	 * for json marshalling use
	 */
	@SuppressWarnings("unused")
	private DeviceStatus() {
		// do nothing
	}

	public DeviceStatus(String resource, String status) {
		super(resource, status);
		this.alert = (getStatus().equals(BrotherLaserClient.STATUS_OK) == false);
	}

}
