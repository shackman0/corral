package com.topdesk.iot.devices.dto;

import com.topdesk.iot.devices.IDeviceStatus;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * DeviceStatusDto
 * <p>
 *
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter(value=AccessLevel.PRIVATE)
@Builder
public class DeviceStatusDto implements IDeviceStatus {

	String	resource;
	String	status;
	boolean	alert;

	public DeviceStatusDto(IDeviceStatus deviceStatus) {
		resource = deviceStatus.getResource();
		status = deviceStatus.getStatus();
		alert = deviceStatus.isAlert();
	}
}
