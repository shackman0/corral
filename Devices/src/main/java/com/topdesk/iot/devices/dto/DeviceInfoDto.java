package com.topdesk.iot.devices.dto;

import java.util.Collection;
import java.util.HashSet;

import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;

import flexjson.JSONDeserializer;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter(value=AccessLevel.PRIVATE)
@Builder
public class DeviceInfoDto implements IDeviceInfo<IDeviceStatus> {

	private boolean											ignore;
	private String											host;
	private String											objectId;
	private String											branchOffice;
	private String											manufacturer;
	private String											model;
	@Setter
	private int													httpResponseCode;
	private Collection<IDeviceStatus>		deviceStatuses;

	public DeviceInfoDto (IDeviceInfo<? extends IDeviceStatus> deviceInfo) {
		host = deviceInfo.getHost();
		objectId = deviceInfo.getObjectId();
		branchOffice = deviceInfo.getBranchOffice();
		manufacturer = deviceInfo.getManufacturer();
		model = deviceInfo.getModel();
		deviceStatuses = new HashSet<>();
		for (IDeviceStatus deviceStatus: deviceInfo.getDeviceStatuses()) {
			deviceStatuses.add(new DeviceStatusDto(deviceStatus));
		}
	}

	public static DeviceInfoDto convertJson2DeviceInfoDto(String json) {
		DeviceInfoDto deviceInfo = new JSONDeserializer<DeviceInfoDto>()
			.use("deviceStatuses.members", HashSet.class)
			.use("deviceStatuses.values", DeviceStatusDto.class)
			.deserialize(json, DeviceInfoDto.class);

		return deviceInfo;
	}

}
