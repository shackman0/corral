package com.topdesk.iot.devices;

import flexjson.JSONSerializer;

public interface IDeviceStatus {

	String getResource();
	String getStatus();
	boolean isAlert();

	default String asJson() {
  	String json = new JSONSerializer()
  			.exclude("*.class")
    		.serialize(this);

  	return json;
	}

}
