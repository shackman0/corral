package com.topdesk.iot.devices;

import java.util.Collection;
import java.util.HashSet;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public interface IDeviceInfo <U extends IDeviceStatus> {

	boolean isIgnore();
	String getHost();

	String getObjectId();
	String getBranchOffice();

	String getManufacturer();
	String getModel();

//	static final int RC_EXCEPTION = -1;
//	static final int RC_UNDEFINED = -2;

//	int getHttpResponseCode();
//	void setHttpResponseCode(int httpResponseCode);

	Collection<U> getDeviceStatuses();

	default void addDeviceStatus(U deviceStatus) {
		getDeviceStatuses().add(deviceStatus);
	}

	/**
	 * at least one device statuse has a true alert
	 * @return
	 */
	default boolean isAlert() {
		return (getDeviceStatuses().stream().anyMatch(d -> d.isAlert() == true));
	}

	default String asJson() {
  	String json = new JSONSerializer()
  			.exclude("*.class")
    		.include("deviceStatuses")
    		.serialize(this);

  	return json;
	}

  static <T extends IDeviceInfo<U>, U extends IDeviceStatus> T asDeviceInfo(String json, Class<T> deviceInfoClass, Class<U> deviceStatusClass) {
  	T deviceInfo = new JSONDeserializer<T>()
  		.use("deviceStatuses.members", HashSet.class)
  		.use("deviceStatuses.values", deviceStatusClass)
  		.deserialize(json, deviceInfoClass);

  	return deviceInfo;
  }

}
