package com.topdesk.iot.devices.resources;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.topdesk.iot.devices.DeviceInfo;
import com.topdesk.iot.devices.IDeviceInfo;
import com.topdesk.iot.devices.IDeviceStatus;


/**
 * Devices
 * <p>
 * A list of all the devices to be monitored
 */
public class Devices {

	File csvFile;

	/**
	 * @param csvFilePath path to the devices csv file
	 */
	public Devices(String csvFilePath) {
		this.csvFile = new File(csvFilePath);
	}

	/**
	 * @param csvFile a csv file
	 */
	public Devices(File csvFile) {
		this.csvFile = csvFile;
	}

	/**
	 *
	 * @return
	 * @throws IOException
	 */
	public Collection<IDeviceInfo<IDeviceStatus>> getDeviceInfos() throws IOException {
		Collection<IDeviceInfo<IDeviceStatus>> deviceInfos = new HashSet<>();
		try (Stream<String> lines = Files.lines(Paths.get(csvFile.getAbsolutePath()))) {
			lines
				.skip(1)   // skip the column headers
				.filter(csvLine -> csvLine.trim().startsWith(","))   // remove any lines to be ignored. ANY non-blank value in column 0 means ignore this line
				.forEach(csvLine -> deviceInfos.add(new DeviceInfo(csvLine)));
		}

		Collection<IDeviceInfo<IDeviceStatus>> sortedDeviceInfos = deviceInfos
			.stream()
			.sorted((info0, info1) -> info0.getObjectId().compareTo(info1.getObjectId()))
			.collect(Collectors.toList());

		return sortedDeviceInfos;
	}

//	public static void main(String... strings) {
//		try {
//			Collection<IDeviceInfo<IDeviceStatus>> deviceInfos = new Devices("../publisher/conf/Devices.csv").getDeviceInfos();
//			deviceInfos.forEach(System.out::println);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			System.exit(0);
//		}
//	}

}
