package com.topdesk.iot.devices;

import lombok.Getter;

@Getter
public abstract class AbstractDeviceStatus implements IDeviceStatus {

	String resource;
	String status;
	protected boolean alert;

	protected AbstractDeviceStatus() {
		// do nothing
	}

	/**
	 * children must set this.alert in your constructor
	 * @param resource
	 * @param status
	 */
	public AbstractDeviceStatus(String resource, String status) {
		this.resource = resource;
		this.status = status;
	}

}
