package com.topdesk.iot.devices;

import java.util.Collection;
import java.util.HashSet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * DeviceInfo
 * <p>
 * A device
 */

@ToString(exclude = "deviceStatuses")
@Getter
@AllArgsConstructor
@Builder
public class DeviceInfo implements IDeviceInfo<IDeviceStatus> {

	static int								IGNORE_IDX				= 0;
	static int								HOST_IDX					= IGNORE_IDX + 1;
	static int								OBJECT_ID_IDX			= HOST_IDX + 1;
	static int								BRANCH_OFFICE_IDX	= OBJECT_ID_IDX + 1;
	static int								MANUFACTURER_IDX	= BRANCH_OFFICE_IDX + 1;
	static int								MODEL_IDX					= MANUFACTURER_IDX + 1;

	boolean										ignore;
	String										host;
	String										objectId;
	String										branchOffice;
	String										manufacturer;
	String										model;
	Collection<IDeviceStatus>	deviceStatuses;

	/**
	 * for json marshalling use
	 */
	@SuppressWarnings("unused")
	private DeviceInfo() {
		// do nothing
	}

	public DeviceInfo(String ignore, String host, String objectId, String branchOffice, String manufacturer, String model) {
		init(ignore, host, objectId, branchOffice, manufacturer, model);
	}

	/**
	 * @param csv:
	 *          CSV format (5 columns): host:port, objectId, branchOffice, manufacturer, model where :port is optional.
	 */
	public DeviceInfo(String csv) {
		String[] split = csv.split(",");
		init(split[IGNORE_IDX].trim(), split[HOST_IDX].trim(), split[OBJECT_ID_IDX].trim(), split[BRANCH_OFFICE_IDX].trim(), split[MANUFACTURER_IDX].trim(), split[MODEL_IDX].trim());
	}

	void init(String ignore, String host, String objectId, String branchOffice, String manufacturer, String model) {
		this.ignore = (ignore.trim().isEmpty() == false);
		this.host = host;
		this.objectId = objectId;
		this.branchOffice = branchOffice;
		this.manufacturer = manufacturer;
		this.model = model;
		deviceStatuses = new HashSet<>();
	}

}
