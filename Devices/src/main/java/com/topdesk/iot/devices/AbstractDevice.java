package com.topdesk.iot.devices;

import java.io.IOException;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.http.HttpStatus;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.topdesk.browserdriver.CloseableBrowserDriver;
import com.topdesk.browserdriver.WebException;
import com.topdesk.iot.devices.resources.Devices;
import com.topdesk.util.LogMessage;
import com.topdesk.util.OutParameter;

/**
 * AbstractDevice
 * <p>
 * extract status information from a device
 */
public abstract class AbstractDevice {

	final Logger LOG = LoggerFactory.getLogger(getClass());

	public static String HTTP	= "http";
	public static String HTTPS	= "https";
	public static String STATUS_PAGE_URL	= "%s://%s/%s";


	/**
	 * read the list of devices and create a DeviceInfo for each device
	 * @return Set<DeviceInfo>
	 * @throws IOException
	 */
	public static Collection<IDeviceInfo<IDeviceStatus>> getDeviceInfos() throws IOException {
		Collection<IDeviceInfo<IDeviceStatus>> deviceInfos = new Devices(com.topdesk.iot.configuration.Configuration.PUBLISHER_DEVICES_FILE_PATH.value()).getDeviceInfos();

		return deviceInfos;
	}

	/**
	 * Update the deviceInfo with its current statuses. Previous statuses are cleared.
	 * @return httpResponseCode
	 */
	public void getDeviceStatuses(CloseableBrowserDriver browserDriver, IDeviceInfo<IDeviceStatus> deviceInfo) {
		String host = deviceInfo.getHost();
		String statusPageUrl = getStatusPageUrl(host);
		try {
			OutParameter<Document> domCarrier = new OutParameter<>();
			deviceInfo.getDeviceStatuses().clear();
			LOG.info(LogMessage.msg("====== checking status of " + getClass().getSimpleName() + " @ "  + statusPageUrl + " ======"));
			int httpResponseCode = getStatusPage(host, browserDriver, domCarrier);
			Document document = domCarrier.getPayload();
//			LOG.info(LogMessage.msg(document.toString()));
			if (httpResponseCodeIsOK(httpResponseCode)) {
				Collection<IDeviceStatus> deviceStatuses = extractDeviceStatuses(document);
				deviceInfo.getDeviceStatuses().addAll(deviceStatuses);
			} else {
				LOG.error(LogMessage.msg("HttpResponseCode " + httpResponseCode + " from " + getClass().getSimpleName() + " @ " + statusPageUrl));
			}
		} catch (Exception e) {
			LOG.error(LogMessage.msg("Exception from " + getClass().getSimpleName() + " @ " + statusPageUrl),e);
		}
	}

	protected boolean httpResponseCodeIsOK(int httpResponseCode) {
		return (httpResponseCode == HttpStatus.SC_OK);
	}

	protected String getStatusPageUrl(String host) {
		String statusPageUrl = String.format(STATUS_PAGE_URL, getProtocol(), host, getStatusPagePath());
		return statusPageUrl;
	}

	int getStatusPage(String host, CloseableBrowserDriver browserDriver, OutParameter<Document> domCarrier) throws WebException, IOException, TransformerException, SAXException, ParserConfigurationException {
		String statusPageUrl = getStatusPageUrl(host);
		int httpResponseCode = browserDriver.browse(statusPageUrl, domCarrier);

		return httpResponseCode;
	}

	protected String getProtocol() {
		return HTTPS;
	}

	/**
	 * Asks the device monitor if it can handle a particular device
	 * @return true if the Device monitor can handle this particular device; false otherwise
	 */
	public abstract boolean canQueryThisDevice(IDeviceInfo<IDeviceStatus> deviceInfo);

	/**
	 * @param host
	 * @return return the url of the status page for the specified host
	 */
	protected abstract String getStatusPagePath();

	/**
	 * extract the device statuses from the dom
	 * @param dom the web page containing the statuses
	 * @return DeviceStatuses
	 */
	protected abstract Collection<IDeviceStatus> extractDeviceStatuses(Document dom);

}
